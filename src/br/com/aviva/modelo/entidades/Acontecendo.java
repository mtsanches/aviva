package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="acontecendo")
public class Acontecendo implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_postagem;
	
	private String caminho_foto1;
	private String caminho_foto2;
	private String caminho_foto3;
	private String caminho_foto4;
	private String caminho_foto5;
	private String caminho_foto6;
	private String caminho_foto7;
	private String caminho_foto8;
	private String caminho_foto9;
	private String caminho_foto10;
	
	private String descricao;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	@ManyToOne @JoinColumn(name = "turma_id") 
	private Turma turma;
	
	
	// GETTERS E SETTERS
	
	public Acontecendo(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_postagem() {
		return data_postagem;
	}

	public void setData_postagem(Date data_postagem) {
		this.data_postagem = data_postagem;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public String getCaminho_foto1() {
		return caminho_foto1;
	}

	public void setCaminho_foto1(String caminho_foto1) {
		this.caminho_foto1 = caminho_foto1;
	}

	public String getCaminho_foto2() {
		return caminho_foto2;
	}

	public void setCaminho_foto2(String caminho_foto2) {
		this.caminho_foto2 = caminho_foto2;
	}

	public String getCaminho_foto3() {
		return caminho_foto3;
	}

	public void setCaminho_foto3(String caminho_foto3) {
		this.caminho_foto3 = caminho_foto3;
	}

	public String getCaminho_foto4() {
		return caminho_foto4;
	}

	public void setCaminho_foto4(String caminho_foto4) {
		this.caminho_foto4 = caminho_foto4;
	}

	public String getCaminho_foto5() {
		return caminho_foto5;
	}

	public void setCaminho_foto5(String caminho_foto5) {
		this.caminho_foto5 = caminho_foto5;
	}

	public String getCaminho_foto6() {
		return caminho_foto6;
	}

	public void setCaminho_foto6(String caminho_foto6) {
		this.caminho_foto6 = caminho_foto6;
	}

	public String getCaminho_foto7() {
		return caminho_foto7;
	}

	public void setCaminho_foto7(String caminho_foto7) {
		this.caminho_foto7 = caminho_foto7;
	}

	public String getCaminho_foto8() {
		return caminho_foto8;
	}

	public void setCaminho_foto8(String caminho_foto8) {
		this.caminho_foto8 = caminho_foto8;
	}

	public String getCaminho_foto9() {
		return caminho_foto9;
	}

	public void setCaminho_foto9(String caminho_foto9) {
		this.caminho_foto9 = caminho_foto9;
	}

	public String getCaminho_foto10() {
		return caminho_foto10;
	}

	public void setCaminho_foto10(String caminho_foto10) {
		this.caminho_foto10 = caminho_foto10;
	}
	
		
	
}
