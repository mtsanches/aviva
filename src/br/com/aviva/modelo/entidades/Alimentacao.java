package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="alimentacao")
public class Alimentacao implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_ocorrencia;
	
	private String tipo_alimento;
	private String nome_alimento;
	private String status_alimento;
	
	private String periodo_alimento;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	// GETTERS E SETTERS
	
	public Alimentacao(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_ocorrencia() {
		return data_ocorrencia;
	}

	public void setData_ocorrencia(Date data_ocorrencia) {
		this.data_ocorrencia = data_ocorrencia;
	}

	public String getTipo_alimento() {
		return tipo_alimento;
	}

	public void setTipo_alimento(String tipo_alimento) {
		this.tipo_alimento = tipo_alimento;
	}

	public String getNome_alimento() {
		return nome_alimento;
	}

	public void setNome_alimento(String nome_alimento) {
		this.nome_alimento = nome_alimento;
	}

	public String getPeriodo_alimento() {
		return periodo_alimento;
	}

	public void setPeriodo_alimento(String periodo_alimento) {
		this.periodo_alimento = periodo_alimento;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getStatus_alimento() {
		return status_alimento;
	}

	public void setStatus_alimento(String status_alimento) {
		this.status_alimento = status_alimento;
	}

	
		
	
}
