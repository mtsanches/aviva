package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="mochila")
public class Mochila implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_ocorrencia;
	
	private String recado;
	private String status;	

	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	// GETTERS E SETTERS
	
	public Mochila(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_ocorrencia() {
		return data_ocorrencia;
	}

	public void setData_ocorrencia(Date data_ocorrencia) {
		this.data_ocorrencia = data_ocorrencia;
	}

	public String getRecado() {
		return recado;
	}

	public void setRecado(String recado) {
		this.recado = recado;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	
	

	
		
	
}
