package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="valor")
public class Valor implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	private String tipo_valor;
	private BigDecimal valor;
	
	private int horas_periodo;
	private int ano;
	
	@ManyToOne @JoinColumn(name = "escola_id") 
	private Escola escola;
	
	@ManyToOne @JoinColumn(name = "turma_id") 
	private Turma turma;
	
	@ManyToOne @JoinColumn(name = "periodo_id") 
	private Periodo periodo;
	
	private BigDecimal desconto_anuidade;
	private BigDecimal desconto_semestre;
	private BigDecimal desconto_irmao;
	private BigDecimal desconto_matricula;

	
	// GETTERS E SETTERS
	
	public Valor(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo_valor() {
		return tipo_valor;
	}

	public void setTipo_valor(String tipo_valor) {
		this.tipo_valor = tipo_valor;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public int getHoras_periodo() {
		return horas_periodo;
	}

	public void setHoras_periodo(int horas_periodo) {
		this.horas_periodo = horas_periodo;
	}

	public int getAno() {
		return ano;
	}

	public void setAno(int ano) {
		this.ano = ano;
	}

	public Escola getEscola() {
		return escola;
	}

	public void setEscola(Escola escola) {
		this.escola = escola;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public BigDecimal getDesconto_anuidade() {
		return desconto_anuidade;
	}

	public void setDesconto_anuidade(BigDecimal desconto_anuidade) {
		this.desconto_anuidade = desconto_anuidade;
	}

	public BigDecimal getDesconto_semestre() {
		return desconto_semestre;
	}

	public void setDesconto_semestre(BigDecimal desconto_semestre) {
		this.desconto_semestre = desconto_semestre;
	}

	public BigDecimal getDesconto_irmao() {
		return desconto_irmao;
	}

	public void setDesconto_irmao(BigDecimal desconto_irmao) {
		this.desconto_irmao = desconto_irmao;
	}

	public BigDecimal getDesconto_matricula() {
		return desconto_matricula;
	}

	public void setDesconto_matricula(BigDecimal desconto_matricula) {
		this.desconto_matricula = desconto_matricula;
	}

	
	
	
	
	
}
