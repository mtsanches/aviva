package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="dadosescolares")
public class DadosEscolares implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	private String horario_entrada;
	private String horario_saida;
	
	private Boolean lanche_manha;
	private Boolean lanche_tarde;
	private Boolean almoco;
	private Boolean jantar;
	
	private Boolean flag_horario_misto;
	private String seg_horario_entrada;
	private String seg_horario_saida;
	private String ter_horario_entrada;
	private String ter_horario_saida;
	private String qua_horario_entrada;
	private String qua_horario_saida;
	private String qui_horario_entrada;
	private String qui_horario_saida;
	private String sex_horario_entrada;
	private String sex_horario_saida;
	
	
	// GETTERS E SETTERS
	
	public DadosEscolares(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getHorario_entrada() {
		return horario_entrada;
	}

	public void setHorario_entrada(String horario_entrada) {
		this.horario_entrada = horario_entrada;
	}

	public String getHorario_saida() {
		return horario_saida;
	}

	public void setHorario_saida(String horario_saida) {
		this.horario_saida = horario_saida;
	}

	public Boolean getLanche_manha() {
		return lanche_manha;
	}

	public void setLanche_manha(Boolean lanche_manha) {
		this.lanche_manha = lanche_manha;
	}

	public Boolean getLanche_tarde() {
		return lanche_tarde;
	}

	public void setLanche_tarde(Boolean lanche_tarde) {
		this.lanche_tarde = lanche_tarde;
	}

	public Boolean getAlmoco() {
		return almoco;
	}

	public void setAlmoco(Boolean almoco) {
		this.almoco = almoco;
	}

	public Boolean getJantar() {
		return jantar;
	}

	public void setJantar(Boolean jantar) {
		this.jantar = jantar;
	}

	public Boolean getFlag_horario_misto() {
		return flag_horario_misto;
	}

	public void setFlag_horario_misto(Boolean flag_horario_misto) {
		this.flag_horario_misto = flag_horario_misto;
	}

	public String getSeg_horario_entrada() {
		return seg_horario_entrada;
	}

	public void setSeg_horario_entrada(String seg_horario_entrada) {
		this.seg_horario_entrada = seg_horario_entrada;
	}

	public String getSeg_horario_saida() {
		return seg_horario_saida;
	}

	public void setSeg_horario_saida(String seg_horario_saida) {
		this.seg_horario_saida = seg_horario_saida;
	}

	public String getTer_horario_entrada() {
		return ter_horario_entrada;
	}

	public void setTer_horario_entrada(String ter_horario_entrada) {
		this.ter_horario_entrada = ter_horario_entrada;
	}

	public String getTer_horario_saida() {
		return ter_horario_saida;
	}

	public void setTer_horario_saida(String ter_horario_saida) {
		this.ter_horario_saida = ter_horario_saida;
	}

	public String getQua_horario_entrada() {
		return qua_horario_entrada;
	}

	public void setQua_horario_entrada(String qua_horario_entrada) {
		this.qua_horario_entrada = qua_horario_entrada;
	}

	public String getQua_horario_saida() {
		return qua_horario_saida;
	}

	public void setQua_horario_saida(String qua_horario_saida) {
		this.qua_horario_saida = qua_horario_saida;
	}

	public String getQui_horario_entrada() {
		return qui_horario_entrada;
	}

	public void setQui_horario_entrada(String qui_horario_entrada) {
		this.qui_horario_entrada = qui_horario_entrada;
	}

	public String getQui_horario_saida() {
		return qui_horario_saida;
	}

	public void setQui_horario_saida(String qui_horario_saida) {
		this.qui_horario_saida = qui_horario_saida;
	}

	public String getSex_horario_entrada() {
		return sex_horario_entrada;
	}

	public void setSex_horario_entrada(String sex_horario_entrada) {
		this.sex_horario_entrada = sex_horario_entrada;
	}

	public String getSex_horario_saida() {
		return sex_horario_saida;
	}

	public void setSex_horario_saida(String sex_horario_saida) {
		this.sex_horario_saida = sex_horario_saida;
	}

			
	
}
