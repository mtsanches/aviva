package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="mensagem")
public class Mensagem implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne @JoinColumn(name = "emissor_id") 
	private Usuario emissor;
	
	@ManyToOne @JoinColumn(name = "destinatario_id") 
	private Usuario destinatario;
	
	private String assunto;
	private String texto_mensagem;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_leitura;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_exclusao;

	private Boolean flag_lida;
	private Boolean flag_removida;
	private Boolean flag_anexo;
	
	private String tipo_mensagem;
	
	// GETTERS E SETTERS
	
	public Mensagem(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Usuario getEmissor() {
		return emissor;
	}

	public void setEmissor(Usuario emissor) {
		this.emissor = emissor;
	}

	public Usuario getDestinatario() {
		return destinatario;
	}

	public void setDestinatario(Usuario destinatario) {
		this.destinatario = destinatario;
	}

	public String getTexto_mensagem() {
		return texto_mensagem;
	}

	public void setTexto_mensagem(String texto_mensagem) {
		this.texto_mensagem = texto_mensagem;
	}

	public Date getData_criacao() {
		return data_criacao;
	}

	public void setData_criacao(Date data_criacao) {
		this.data_criacao = data_criacao;
	}

	public Date getData_leitura() {
		return data_leitura;
	}

	public void setData_leitura(Date data_leitura) {
		this.data_leitura = data_leitura;
	}

	public Date getData_exclusao() {
		return data_exclusao;
	}

	public void setData_exclusao(Date data_exclusao) {
		this.data_exclusao = data_exclusao;
	}

	public Boolean getFlag_lida() {
		return flag_lida;
	}

	public void setFlag_lida(Boolean flag_lida) {
		this.flag_lida = flag_lida;
	}

	public Boolean getFlag_removida() {
		return flag_removida;
	}

	public void setFlag_removida(Boolean flag_removida) {
		this.flag_removida = flag_removida;
	}

	public String getAssunto() {
		return assunto;
	}

	public void setAssunto(String assunto) {
		this.assunto = assunto;
	}

	public Boolean getFlag_anexo() {
		return flag_anexo;
	}

	public void setFlag_anexo(Boolean flag_anexo) {
		this.flag_anexo = flag_anexo;
	}

	public String getTipo_mensagem() {
		return tipo_mensagem;
	}

	public void setTipo_mensagem(String tipo_mensagem) {
		this.tipo_mensagem = tipo_mensagem;
	}

	
	

		
	
}
