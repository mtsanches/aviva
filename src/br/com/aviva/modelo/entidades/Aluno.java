package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;
import org.hibernate.mapping.Set;

@Entity
@Table(name="aluno")
public class Aluno implements Serializable {

	@Id @GeneratedValue
	private Long id;

	private int codigo;
	
	@Index(name = "nome_INDEX")
	private String nome ;
	
	private Date data_nascimento;
	private String genero;
	
	private String foto;
	
	@ManyToOne @JoinColumn(name = "escola_id") 
	private Escola escola;
	
	@ManyToOne @JoinColumn(name = "unidade_id") 
	private Unidade unidade;
	
	@ManyToOne @JoinColumn(name = "turma_id") 
	private Turma turma;

	@ManyToOne @JoinColumn(name = "periodo_id") 
	private Periodo periodo;
	
	private String tipo_sanguineo;
	
	@ManyToOne @JoinColumn(name = "planomedico_id") 
	private PlanoMedico planomedico;

	private String historico;
	private String status;
	
	private int defasagem_anual;
	
	//endere�o
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String estado;
	private String cep;
		
	private Boolean flag_ativo;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	
	// GETTERS E SETTERS
	
	public Aluno(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public int getCodigo() {
		return codigo;
	}

	public void setCodigo(int codigo) {
		this.codigo = codigo;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public Date getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(Date data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public String getGenero() {
		return genero;
	}

	public void setGenero(String genero) {
		this.genero = genero;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Escola getEscola() {
		return escola;
	}

	public void setEscola(Escola escola) {
		this.escola = escola;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Periodo getPeriodo() {
		return periodo;
	}

	public void setPeriodo(Periodo periodo) {
		this.periodo = periodo;
	}

	public String getTipo_sanguineo() {
		return tipo_sanguineo;
	}

	public void setTipo_sanguineo(String tipo_sanguineo) {
		this.tipo_sanguineo = tipo_sanguineo;
	}

	public PlanoMedico getPlanomedico() {
		return planomedico;
	}

	public void setPlanomedico(PlanoMedico planomedico) {
		this.planomedico = planomedico;
	}

	public String getHistorico() {
		return historico;
	}

	public void setHistorico(String historico) {
		this.historico = historico;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public int getDefasagem_anual() {
		return defasagem_anual;
	}

	public void setDefasagem_anual(int defasagem_anual) {
		this.defasagem_anual = defasagem_anual;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public Boolean getFlag_ativo() {
		return flag_ativo;
	}

	public void setFlag_ativo(Boolean flag_ativo) {
		this.flag_ativo = flag_ativo;
	}

	public Date getData_criacao() {
		return data_criacao;
	}

	public void setData_criacao(Date data_criacao) {
		this.data_criacao = data_criacao;
	}

		
	
}
