package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="liquido")
public class Liquido implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_ocorrencia;
	
	private String tipo_liquido;
	private String nome_liquido;
	private String quantidade;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	// GETTERS E SETTERS
	
	public Liquido(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_ocorrencia() {
		return data_ocorrencia;
	}

	public void setData_ocorrencia(Date data_ocorrencia) {
		this.data_ocorrencia = data_ocorrencia;
	}

	public String getTipo_liquido() {
		return tipo_liquido;
	}

	public void setTipo_liquido(String tipo_liquido) {
		this.tipo_liquido = tipo_liquido;
	}

	public String getNome_liquido() {
		return nome_liquido;
	}

	public void setNome_liquido(String nome_liquido) {
		this.nome_liquido = nome_liquido;
	}

	public String getQuantidade() {
		return quantidade;
	}

	public void setQuantidade(String quantidade) {
		this.quantidade = quantidade;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	
	
		
	
}
