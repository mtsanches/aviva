package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="autorizacao")
public class Autorizacao implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_autorizacao;
		
	private String descricao;
	private String foto;
	private Boolean aberta;
		
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	private String nome_responsavel;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_evento;
	
	private String local;
	private String website;
	
	
	// GETTERS E SETTERS
	
	public Autorizacao(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_autorizacao() {
		return data_autorizacao;
	}

	public void setData_autorizacao(Date data_autorizacao) {
		this.data_autorizacao = data_autorizacao;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	

	public Boolean getAberta() {
		return aberta;
	}

	public void setAberta(Boolean aberta) {
		this.aberta = aberta;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getNome_responsavel() {
		return nome_responsavel;
	}

	public void setNome_responsavel(String nome_responsavel) {
		this.nome_responsavel = nome_responsavel;
	}

	public Date getData_evento() {
		return data_evento;
	}

	public void setData_evento(Date data_evento) {
		this.data_evento = data_evento;
	}

	public String getLocal() {
		return local;
	}

	public void setLocal(String local) {
		this.local = local;
	}

	public String getWebsite() {
		return website;
	}

	public void setWebsite(String website) {
		this.website = website;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

			
	
}
