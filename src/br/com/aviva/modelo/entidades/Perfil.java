package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Index;

@Entity
@Table(name="perfil")
public class Perfil implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	private String nome_perfil ;
	private Boolean flag_admin;
	private Boolean flag_colaborador;
	private Boolean flag_usuario;
	private Boolean flag_webroot;
	
	// GETTERS E SETTERS
	
	public Perfil(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome_perfil() {
		return nome_perfil;
	}

	public void setNome_perfil(String nome_perfil) {
		this.nome_perfil = nome_perfil;
	}

	public Boolean getFlag_admin() {
		return flag_admin;
	}

	public void setFlag_admin(Boolean flag_admin) {
		this.flag_admin = flag_admin;
	}

	public Boolean getFlag_colaborador() {
		return flag_colaborador;
	}

	public void setFlag_colaborador(Boolean flag_colaborador) {
		this.flag_colaborador = flag_colaborador;
	}

	public Boolean getFlag_usuario() {
		return flag_usuario;
	}

	public void setFlag_usuario(Boolean flag_usuario) {
		this.flag_usuario = flag_usuario;
	}

	public Boolean getFlag_webroot() {
		return flag_webroot;
	}

	public void setFlag_webroot(Boolean flag_webroot) {
		this.flag_webroot = flag_webroot;
	}

	

		
	
}
