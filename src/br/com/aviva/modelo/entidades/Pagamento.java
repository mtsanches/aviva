package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="pagamento")
public class Pagamento implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.DATE)
	private Date data_vencimento;
	
	private BigDecimal valor;
	
	private String descricao;
	private String status;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;

	private String linha_digitavel;
	private String banco_conta;
	private String tipo_pagamento;
	
	private BigDecimal desconto;
	
	@Basic
	@Temporal(TemporalType.DATE)
	private Date data_pagamento;
	
	private String referencia;
	
	private BigDecimal valor_pago;
	private BigDecimal juros;
	private BigDecimal multa;
	
	// GETTERS E SETTERS
	
	public Pagamento(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_vencimento() {
		return data_vencimento;
	}

	public void setData_vencimento(Date data_vencimento) {
		this.data_vencimento = data_vencimento;
	}

	public BigDecimal getValor() {
		return valor;
	}

	public void setValor(BigDecimal valor) {
		this.valor = valor;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getLinha_digitavel() {
		return linha_digitavel;
	}

	public void setLinha_digitavel(String linha_digitavel) {
		this.linha_digitavel = linha_digitavel;
	}

	public String getBanco_conta() {
		return banco_conta;
	}

	public void setBanco_conta(String banco_conta) {
		this.banco_conta = banco_conta;
	}

	public String getTipo_pagamento() {
		return tipo_pagamento;
	}

	public void setTipo_pagamento(String tipo_pagamento) {
		this.tipo_pagamento = tipo_pagamento;
	}

	public BigDecimal getDesconto() {
		return desconto;
	}

	public void setDesconto(BigDecimal desconto) {
		this.desconto = desconto;
	}

	public Date getData_pagamento() {
		return data_pagamento;
	}

	public void setData_pagamento(Date data_pagamento) {
		this.data_pagamento = data_pagamento;
	}

	public String getReferencia() {
		return referencia;
	}

	public void setReferencia(String referencia) {
		this.referencia = referencia;
	}

	public BigDecimal getValor_pago() {
		return valor_pago;
	}

	public void setValor_pago(BigDecimal valor_pago) {
		this.valor_pago = valor_pago;
	}

	public BigDecimal getJuros() {
		return juros;
	}

	public void setJuros(BigDecimal juros) {
		this.juros = juros;
	}

	public BigDecimal getMulta() {
		return multa;
	}

	public void setMulta(BigDecimal multa) {
		this.multa = multa;
	}

	
	
	
	
}
