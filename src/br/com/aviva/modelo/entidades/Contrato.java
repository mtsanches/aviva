package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="contrato")
public class Contrato implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	//DADOS DO ACESSO
	private String login;
	private String cpf;
	private String data_nascimento;
	private Boolean flag_primeiro_acesso;
	private Boolean flag_inadimplente;
	private String endereco_ip;
	
	//DADOS ESCOLARES
	@Index(name = "nome_aluno_INDEX")
	private String nome_aluno;
	private String grupo;
	private String turma;
	private String periodo;
	private String hora_entrada;
	private String hora_saida;
	private String total_horas;
	private Boolean horario_misto;
	private String lanche;
	private String refeicao;
	
	private String horario_misto_entrada_seg;
	private String horario_misto_saida_seg;
	private String periodo_seg;
	private String lanche_misto_seg;
	private String refeicao_misto_seg;
	
	private String horario_misto_entrada_ter;
	private String horario_misto_saida_ter;
	private String periodo_ter;
	private String lanche_misto_ter;
	private String refeicao_misto_ter;
	
	private String horario_misto_entrada_qua;
	private String horario_misto_saida_qua;
	private String periodo_qua;
	private String lanche_misto_qua;
	private String refeicao_misto_qua;
	
	private String horario_misto_entrada_qui;
	private String horario_misto_saida_qui;
	private String periodo_qui;
	private String lanche_misto_qui;
	private String refeicao_misto_qui;
	
	private String horario_misto_entrada_sex;
	private String horario_misto_saida_sex;
	private String periodo_sex;
	private String lanche_misto_sex;
	private String refeicao_misto_sex;

	//DADOS CONTRATANTE
	private String nome_contratante;
	private String email;
	private String celular;
	private String cep;
	private String endereco;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String uf;

	private String data_inicio;
	
	private String mes_inicio;
	private Double valor_contrato;
	
	private Boolean mensalidade;
	private Double mensalidade_valor_total;
	private Double mensalidade_matricula;
	private Double mensalidade_valor_parcela;
	
	private Boolean semestralidade;
	private Double semestralidade_valor_total;
	private Double semestralidade_matricula;
	private Double semestralidade_valor_parcela;
	
	private Boolean anuidade8;
	private Double anuidade8_valor_total;
	private Double anuidade8_matricula;
	private Double anuidade8_valor_parcela;
	
	private Boolean anuidade10;
	private Double anuidade10_valor_total;
	private Double anuidade10_matricula;
	private Double anuidade10_valor_parcela;
	
	//DADOS MATRICULA
	private Boolean matricula_avista10;
	private Double matricula_avista10_valor;
	private String matricula_avista10_pagamento;
	private String matricula_avista10_vencimento_boleto;
	
	private Boolean matricula_avista6;
	private Double matricula_avista6_valor;
	private String matricula_avista6_pagamento;
	private String matricula_avista6_vencimento_boleto;
	
	private Boolean matricula_3parcelas;
	private Double matricula_3parcelas_valor;
	private String matricula_3parcelas_vencimento_boleto1;
	private String matricula_3parcelas_vencimento_boleto2;
	private String matricula_3parcelas_vencimento_boleto3;
	
	//DADOS MATERIAL
	private Boolean material_1parcela;
	private Double material_1parcela_valor;
	private String material_1parcela_vencimento_boleto;
	
	private Boolean material_3parcelas;
	private Double material_3parcelas_valor;
	private String material_3parcelas_vencimento_boleto1;
	private String material_3parcelas_vencimento_boleto2;
	private String material_3parcelas_vencimento_boleto3;
	
	//DADOS ATIVIDADES EXTRA CURRICULARES
	private Boolean atividade_bale;
	private Boolean atividade_bale_1parcela;
	private Double atividade_bale_1parcela_valor;
	private String atividade_bale_1parcela_boleto;
	private Boolean atividade_bale_10parcelas;
	private Double atividade_bale_10parcelas_valor;
	private String atividade_bale_10parcelas_boleto;
	
	private Boolean atividade_judo;
	private Boolean atividade_judo_1parcela;
	private Double atividade_judo_1parcela_valor;
	private String atividade_judo_1parcela_boleto;
	private Boolean atividade_judo_10parcelas;
	private Double atividade_judo_10parcelas_valor;
	private String atividade_judo_10parcelas_boleto;
	
	private Boolean uso_imagem;
	
	private Boolean flag_concordo_contrato;
	private Boolean flag_periodo_alterado;
	private String texto_periodo_alterado;
	private String historico_texto_periodo_alterado;
	private Boolean flag_boleto_emitido;
	
	
	private String status_contrato;
	private String unidade;
	
	private Double valor_base;
	private String desconto;
	private String ano_entrada;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_contratacao;
	
	
	// GETTERS E SETTERS
	
	public Contrato(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(String data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public String getNome_aluno() {
		return nome_aluno;
	}

	public void setNome_aluno(String nome_aluno) {
		this.nome_aluno = nome_aluno;
	}

	public String getGrupo() {
		return grupo;
	}

	public void setGrupo(String grupo) {
		this.grupo = grupo;
	}

	public String getTurma() {
		return turma;
	}

	public void setTurma(String turma) {
		this.turma = turma;
	}

	public String getPeriodo() {
		return periodo;
	}

	public void setPeriodo(String periodo) {
		this.periodo = periodo;
	}

	public String getHora_entrada() {
		return hora_entrada;
	}

	public void setHora_entrada(String hora_entrada) {
		this.hora_entrada = hora_entrada;
	}

	public String getHora_saida() {
		return hora_saida;
	}

	public void setHora_saida(String hora_saida) {
		this.hora_saida = hora_saida;
	}

	public String getTotal_horas() {
		return total_horas;
	}

	public void setTotal_horas(String total_horas) {
		this.total_horas = total_horas;
	}

	public Boolean getHorario_misto() {
		return horario_misto;
	}

	public void setHorario_misto(Boolean horario_misto) {
		this.horario_misto = horario_misto;
	}

	public String getLanche() {
		return lanche;
	}

	public void setLanche(String lanche) {
		this.lanche = lanche;
	}

	public String getRefeicao() {
		return refeicao;
	}

	public void setRefeicao(String refeicao) {
		this.refeicao = refeicao;
	}

	public String getHorario_misto_entrada_seg() {
		return horario_misto_entrada_seg;
	}

	public void setHorario_misto_entrada_seg(String horario_misto_entrada_seg) {
		this.horario_misto_entrada_seg = horario_misto_entrada_seg;
	}

	public String getHorario_misto_saida_seg() {
		return horario_misto_saida_seg;
	}

	public void setHorario_misto_saida_seg(String horario_misto_saida_seg) {
		this.horario_misto_saida_seg = horario_misto_saida_seg;
	}

	public String getLanche_misto_seg() {
		return lanche_misto_seg;
	}

	public void setLanche_misto_seg(String lanche_misto_seg) {
		this.lanche_misto_seg = lanche_misto_seg;
	}

	public String getRefeicao_misto_seg() {
		return refeicao_misto_seg;
	}

	public void setRefeicao_misto_seg(String refeicao_misto_seg) {
		this.refeicao_misto_seg = refeicao_misto_seg;
	}

	public String getHorario_misto_entrada_ter() {
		return horario_misto_entrada_ter;
	}

	public void setHorario_misto_entrada_ter(String horario_misto_entrada_ter) {
		this.horario_misto_entrada_ter = horario_misto_entrada_ter;
	}

	public String getHorario_misto_saida_ter() {
		return horario_misto_saida_ter;
	}

	public void setHorario_misto_saida_ter(String horario_misto_saida_ter) {
		this.horario_misto_saida_ter = horario_misto_saida_ter;
	}

	public String getLanche_misto_ter() {
		return lanche_misto_ter;
	}

	public void setLanche_misto_ter(String lanche_misto_ter) {
		this.lanche_misto_ter = lanche_misto_ter;
	}

	public String getRefeicao_misto_ter() {
		return refeicao_misto_ter;
	}

	public void setRefeicao_misto_ter(String refeicao_misto_ter) {
		this.refeicao_misto_ter = refeicao_misto_ter;
	}

	public String getHorario_misto_entrada_qua() {
		return horario_misto_entrada_qua;
	}

	public void setHorario_misto_entrada_qua(String horario_misto_entrada_qua) {
		this.horario_misto_entrada_qua = horario_misto_entrada_qua;
	}

	public String getHorario_misto_saida_qua() {
		return horario_misto_saida_qua;
	}

	public void setHorario_misto_saida_qua(String horario_misto_saida_qua) {
		this.horario_misto_saida_qua = horario_misto_saida_qua;
	}

	public String getLanche_misto_qua() {
		return lanche_misto_qua;
	}

	public void setLanche_misto_qua(String lanche_misto_qua) {
		this.lanche_misto_qua = lanche_misto_qua;
	}

	public String getRefeicao_misto_qua() {
		return refeicao_misto_qua;
	}

	public void setRefeicao_misto_qua(String refeicao_misto_qua) {
		this.refeicao_misto_qua = refeicao_misto_qua;
	}

	public String getHorario_misto_entrada_qui() {
		return horario_misto_entrada_qui;
	}

	public void setHorario_misto_entrada_qui(String horario_misto_entrada_qui) {
		this.horario_misto_entrada_qui = horario_misto_entrada_qui;
	}

	public String getHorario_misto_saida_qui() {
		return horario_misto_saida_qui;
	}

	public void setHorario_misto_saida_qui(String horario_misto_saida_qui) {
		this.horario_misto_saida_qui = horario_misto_saida_qui;
	}

	public String getLanche_misto_qui() {
		return lanche_misto_qui;
	}

	public void setLanche_misto_qui(String lanche_misto_qui) {
		this.lanche_misto_qui = lanche_misto_qui;
	}

	public String getRefeicao_misto_qui() {
		return refeicao_misto_qui;
	}

	public void setRefeicao_misto_qui(String refeicao_misto_qui) {
		this.refeicao_misto_qui = refeicao_misto_qui;
	}

	public String getHorario_misto_entrada_sex() {
		return horario_misto_entrada_sex;
	}

	public void setHorario_misto_entrada_sex(String horario_misto_entrada_sex) {
		this.horario_misto_entrada_sex = horario_misto_entrada_sex;
	}

	public String getHorario_misto_saida_sex() {
		return horario_misto_saida_sex;
	}

	public void setHorario_misto_saida_sex(String horario_misto_saida_sex) {
		this.horario_misto_saida_sex = horario_misto_saida_sex;
	}

	public String getLanche_misto_sex() {
		return lanche_misto_sex;
	}

	public void setLanche_misto_sex(String lanche_misto_sex) {
		this.lanche_misto_sex = lanche_misto_sex;
	}

	public String getRefeicao_misto_sex() {
		return refeicao_misto_sex;
	}

	public void setRefeicao_misto_sex(String refeicao_misto_sex) {
		this.refeicao_misto_sex = refeicao_misto_sex;
	}

	public String getNome_contratante() {
		return nome_contratante;
	}

	public void setNome_contratante(String nome_contratante) {
		this.nome_contratante = nome_contratante;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getCelular() {
		return celular;
	}

	public void setCelular(String celular) {
		this.celular = celular;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getEndereco() {
		return endereco;
	}

	public void setEndereco(String endereco) {
		this.endereco = endereco;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getUf() {
		return uf;
	}

	public void setUf(String uf) {
		this.uf = uf;
	}

	public String getData_inicio() {
		return data_inicio;
	}

	public void setData_inicio(String data_inicio) {
		this.data_inicio = data_inicio;
	}

	public Double getValor_contrato() {
		return valor_contrato;
	}

	public void setValor_contrato(Double valor_contrato) {
		this.valor_contrato = valor_contrato;
	}

	public Boolean getMensalidade() {
		return mensalidade;
	}

	public void setMensalidade(Boolean mensalidade) {
		this.mensalidade = mensalidade;
	}

	public Double getMensalidade_valor_total() {
		return mensalidade_valor_total;
	}

	public void setMensalidade_valor_total(Double mensalidade_valor_total) {
		this.mensalidade_valor_total = mensalidade_valor_total;
	}

	public Double getMensalidade_matricula() {
		return mensalidade_matricula;
	}

	public void setMensalidade_matricula(Double mensalidade_matricula) {
		this.mensalidade_matricula = mensalidade_matricula;
	}

	public Double getMensalidade_valor_parcela() {
		return mensalidade_valor_parcela;
	}

	public void setMensalidade_valor_parcela(Double mensalidade_valor_parcela) {
		this.mensalidade_valor_parcela = mensalidade_valor_parcela;
	}

	public Boolean getSemestralidade() {
		return semestralidade;
	}

	public void setSemestralidade(Boolean semestralidade) {
		this.semestralidade = semestralidade;
	}

	public Double getSemestralidade_valor_total() {
		return semestralidade_valor_total;
	}

	public void setSemestralidade_valor_total(Double semestralidade_valor_total) {
		this.semestralidade_valor_total = semestralidade_valor_total;
	}

	public Double getSemestralidade_matricula() {
		return semestralidade_matricula;
	}

	public void setSemestralidade_matricula(Double semestralidade_matricula) {
		this.semestralidade_matricula = semestralidade_matricula;
	}

	public Double getSemestralidade_valor_parcela() {
		return semestralidade_valor_parcela;
	}

	public void setSemestralidade_valor_parcela(Double semestralidade_valor_parcela) {
		this.semestralidade_valor_parcela = semestralidade_valor_parcela;
	}

	public Boolean getAnuidade8() {
		return anuidade8;
	}

	public void setAnuidade8(Boolean anuidade8) {
		this.anuidade8 = anuidade8;
	}

	public Double getAnuidade8_valor_total() {
		return anuidade8_valor_total;
	}

	public void setAnuidade8_valor_total(Double anuidade8_valor_total) {
		this.anuidade8_valor_total = anuidade8_valor_total;
	}

	public Double getAnuidade8_matricula() {
		return anuidade8_matricula;
	}

	public void setAnuidade8_matricula(Double anuidade8_matricula) {
		this.anuidade8_matricula = anuidade8_matricula;
	}

	public Double getAnuidade8_valor_parcela() {
		return anuidade8_valor_parcela;
	}

	public void setAnuidade8_valor_parcela(Double anuidade8_valor_parcela) {
		this.anuidade8_valor_parcela = anuidade8_valor_parcela;
	}

	public Boolean getAnuidade10() {
		return anuidade10;
	}

	public void setAnuidade10(Boolean anuidade10) {
		this.anuidade10 = anuidade10;
	}

	public Double getAnuidade10_valor_total() {
		return anuidade10_valor_total;
	}

	public void setAnuidade10_valor_total(Double anuidade10_valor_total) {
		this.anuidade10_valor_total = anuidade10_valor_total;
	}

	public Double getAnuidade10_matricula() {
		return anuidade10_matricula;
	}

	public void setAnuidade10_matricula(Double anuidade10_matricula) {
		this.anuidade10_matricula = anuidade10_matricula;
	}

	public Double getAnuidade10_valor_parcela() {
		return anuidade10_valor_parcela;
	}

	public void setAnuidade10_valor_parcela(Double anuidade10_valor_parcela) {
		this.anuidade10_valor_parcela = anuidade10_valor_parcela;
	}

	public Boolean getMatricula_avista10() {
		return matricula_avista10;
	}

	public void setMatricula_avista10(Boolean matricula_avista10) {
		this.matricula_avista10 = matricula_avista10;
	}

	public Double getMatricula_avista10_valor() {
		return matricula_avista10_valor;
	}

	public void setMatricula_avista10_valor(Double matricula_avista10_valor) {
		this.matricula_avista10_valor = matricula_avista10_valor;
	}

	public String getMatricula_avista10_pagamento() {
		return matricula_avista10_pagamento;
	}

	public void setMatricula_avista10_pagamento(String matricula_avista10_pagamento) {
		this.matricula_avista10_pagamento = matricula_avista10_pagamento;
	}

	public String getMatricula_avista10_vencimento_boleto() {
		return matricula_avista10_vencimento_boleto;
	}

	public void setMatricula_avista10_vencimento_boleto(String matricula_avista10_vencimento_boleto) {
		this.matricula_avista10_vencimento_boleto = matricula_avista10_vencimento_boleto;
	}

	public Boolean getMatricula_avista6() {
		return matricula_avista6;
	}

	public void setMatricula_avista6(Boolean matricula_avista6) {
		this.matricula_avista6 = matricula_avista6;
	}

	public Double getMatricula_avista6_valor() {
		return matricula_avista6_valor;
	}

	public void setMatricula_avista6_valor(Double matricula_avista6_valor) {
		this.matricula_avista6_valor = matricula_avista6_valor;
	}

	public String getMatricula_avista6_pagamento() {
		return matricula_avista6_pagamento;
	}

	public void setMatricula_avista6_pagamento(String matricula_avista6_pagamento) {
		this.matricula_avista6_pagamento = matricula_avista6_pagamento;
	}

	public String getMatricula_avista6_vencimento_boleto() {
		return matricula_avista6_vencimento_boleto;
	}

	public void setMatricula_avista6_vencimento_boleto(String matricula_avista6_vencimento_boleto) {
		this.matricula_avista6_vencimento_boleto = matricula_avista6_vencimento_boleto;
	}

	public Boolean getMatricula_3parcelas() {
		return matricula_3parcelas;
	}

	public void setMatricula_3parcelas(Boolean matricula_3parcelas) {
		this.matricula_3parcelas = matricula_3parcelas;
	}

	public Double getMatricula_3parcelas_valor() {
		return matricula_3parcelas_valor;
	}

	public void setMatricula_3parcelas_valor(Double matricula_3parcelas_valor) {
		this.matricula_3parcelas_valor = matricula_3parcelas_valor;
	}

	public String getMatricula_3parcelas_vencimento_boleto1() {
		return matricula_3parcelas_vencimento_boleto1;
	}

	public void setMatricula_3parcelas_vencimento_boleto1(String matricula_3parcelas_vencimento_boleto1) {
		this.matricula_3parcelas_vencimento_boleto1 = matricula_3parcelas_vencimento_boleto1;
	}

	public String getMatricula_3parcelas_vencimento_boleto2() {
		return matricula_3parcelas_vencimento_boleto2;
	}

	public void setMatricula_3parcelas_vencimento_boleto2(String matricula_3parcelas_vencimento_boleto2) {
		this.matricula_3parcelas_vencimento_boleto2 = matricula_3parcelas_vencimento_boleto2;
	}

	public String getMatricula_3parcelas_vencimento_boleto3() {
		return matricula_3parcelas_vencimento_boleto3;
	}

	public void setMatricula_3parcelas_vencimento_boleto3(String matricula_3parcelas_vencimento_boleto3) {
		this.matricula_3parcelas_vencimento_boleto3 = matricula_3parcelas_vencimento_boleto3;
	}

	public Boolean getMaterial_1parcela() {
		return material_1parcela;
	}

	public void setMaterial_1parcela(Boolean material_1parcela) {
		this.material_1parcela = material_1parcela;
	}

	public Double getMaterial_1parcela_valor() {
		return material_1parcela_valor;
	}

	public void setMaterial_1parcela_valor(Double material_1parcela_valor) {
		this.material_1parcela_valor = material_1parcela_valor;
	}

	public String getMaterial_1parcela_vencimento_boleto() {
		return material_1parcela_vencimento_boleto;
	}

	public void setMaterial_1parcela_vencimento_boleto(String material_1parcela_vencimento_boleto) {
		this.material_1parcela_vencimento_boleto = material_1parcela_vencimento_boleto;
	}

	public Boolean getMaterial_3parcelas() {
		return material_3parcelas;
	}

	public void setMaterial_3parcelas(Boolean material_3parcelas) {
		this.material_3parcelas = material_3parcelas;
	}

	public Double getMaterial_3parcelas_valor() {
		return material_3parcelas_valor;
	}

	public void setMaterial_3parcelas_valor(Double material_3parcelas_valor) {
		this.material_3parcelas_valor = material_3parcelas_valor;
	}

	public String getMaterial_3parcelas_vencimento_boleto1() {
		return material_3parcelas_vencimento_boleto1;
	}

	public void setMaterial_3parcelas_vencimento_boleto1(String material_3parcelas_vencimento_boleto1) {
		this.material_3parcelas_vencimento_boleto1 = material_3parcelas_vencimento_boleto1;
	}

	public String getMaterial_3parcelas_vencimento_boleto2() {
		return material_3parcelas_vencimento_boleto2;
	}

	public void setMaterial_3parcelas_vencimento_boleto2(String material_3parcelas_vencimento_boleto2) {
		this.material_3parcelas_vencimento_boleto2 = material_3parcelas_vencimento_boleto2;
	}

	public String getMaterial_3parcelas_vencimento_boleto3() {
		return material_3parcelas_vencimento_boleto3;
	}

	public void setMaterial_3parcelas_vencimento_boleto3(String material_3parcelas_vencimento_boleto3) {
		this.material_3parcelas_vencimento_boleto3 = material_3parcelas_vencimento_boleto3;
	}

	public Boolean getAtividade_bale() {
		return atividade_bale;
	}

	public void setAtividade_bale(Boolean atividade_bale) {
		this.atividade_bale = atividade_bale;
	}

	public Boolean getAtividade_bale_1parcela() {
		return atividade_bale_1parcela;
	}

	public void setAtividade_bale_1parcela(Boolean atividade_bale_1parcela) {
		this.atividade_bale_1parcela = atividade_bale_1parcela;
	}

	public Double getAtividade_bale_1parcela_valor() {
		return atividade_bale_1parcela_valor;
	}

	public void setAtividade_bale_1parcela_valor(Double atividade_bale_1parcela_valor) {
		this.atividade_bale_1parcela_valor = atividade_bale_1parcela_valor;
	}

	public String getAtividade_bale_1parcela_boleto() {
		return atividade_bale_1parcela_boleto;
	}

	public void setAtividade_bale_1parcela_boleto(String atividade_bale_1parcela_boleto) {
		this.atividade_bale_1parcela_boleto = atividade_bale_1parcela_boleto;
	}

	

	public Boolean getAtividade_judo() {
		return atividade_judo;
	}

	public void setAtividade_judo(Boolean atividade_judo) {
		this.atividade_judo = atividade_judo;
	}

	public Boolean getAtividade_judo_1parcela() {
		return atividade_judo_1parcela;
	}

	public void setAtividade_judo_1parcela(Boolean atividade_judo_1parcela) {
		this.atividade_judo_1parcela = atividade_judo_1parcela;
	}

	public Double getAtividade_judo_1parcela_valor() {
		return atividade_judo_1parcela_valor;
	}

	public void setAtividade_judo_1parcela_valor(Double atividade_judo_1parcela_valor) {
		this.atividade_judo_1parcela_valor = atividade_judo_1parcela_valor;
	}

	public String getAtividade_judo_1parcela_boleto() {
		return atividade_judo_1parcela_boleto;
	}

	public void setAtividade_judo_1parcela_boleto(String atividade_judo_1parcela_boleto) {
		this.atividade_judo_1parcela_boleto = atividade_judo_1parcela_boleto;
	}

	public Boolean getAtividade_bale_10parcelas() {
		return atividade_bale_10parcelas;
	}

	public void setAtividade_bale_10parcelas(Boolean atividade_bale_10parcelas) {
		this.atividade_bale_10parcelas = atividade_bale_10parcelas;
	}

	public Double getAtividade_bale_10parcelas_valor() {
		return atividade_bale_10parcelas_valor;
	}

	public void setAtividade_bale_10parcelas_valor(Double atividade_bale_10parcelas_valor) {
		this.atividade_bale_10parcelas_valor = atividade_bale_10parcelas_valor;
	}

	public String getAtividade_bale_10parcelas_boleto() {
		return atividade_bale_10parcelas_boleto;
	}

	public void setAtividade_bale_10parcelas_boleto(String atividade_bale_10parcelas_boleto) {
		this.atividade_bale_10parcelas_boleto = atividade_bale_10parcelas_boleto;
	}

	public Boolean getAtividade_judo_10parcelas() {
		return atividade_judo_10parcelas;
	}

	public void setAtividade_judo_10parcelas(Boolean atividade_judo_10parcelas) {
		this.atividade_judo_10parcelas = atividade_judo_10parcelas;
	}

	public Double getAtividade_judo_10parcelas_valor() {
		return atividade_judo_10parcelas_valor;
	}

	public void setAtividade_judo_10parcelas_valor(Double atividade_judo_10parcelas_valor) {
		this.atividade_judo_10parcelas_valor = atividade_judo_10parcelas_valor;
	}

	public String getAtividade_judo_10parcelas_boleto() {
		return atividade_judo_10parcelas_boleto;
	}

	public void setAtividade_judo_10parcelas_boleto(String atividade_judo_10parcelas_boleto) {
		this.atividade_judo_10parcelas_boleto = atividade_judo_10parcelas_boleto;
	}

	public Boolean getUso_imagem() {
		return uso_imagem;
	}

	public void setUso_imagem(Boolean uso_imagem) {
		this.uso_imagem = uso_imagem;
	}

	public Boolean getFlag_concordo_contrato() {
		return flag_concordo_contrato;
	}

	public void setFlag_concordo_contrato(Boolean flag_concordo_contrato) {
		this.flag_concordo_contrato = flag_concordo_contrato;
	}

	public Date getData_contratacao() {
		return data_contratacao;
	}

	public void setData_contratacao(Date data_contratacao) {
		this.data_contratacao = data_contratacao;
	}

	public String getPeriodo_seg() {
		return periodo_seg;
	}

	public void setPeriodo_seg(String periodo_seg) {
		this.periodo_seg = periodo_seg;
	}

	public String getPeriodo_ter() {
		return periodo_ter;
	}

	public void setPeriodo_ter(String periodo_ter) {
		this.periodo_ter = periodo_ter;
	}

	public String getPeriodo_qua() {
		return periodo_qua;
	}

	public void setPeriodo_qua(String periodo_qua) {
		this.periodo_qua = periodo_qua;
	}

	public String getPeriodo_qui() {
		return periodo_qui;
	}

	public void setPeriodo_qui(String periodo_qui) {
		this.periodo_qui = periodo_qui;
	}

	public String getPeriodo_sex() {
		return periodo_sex;
	}

	public void setPeriodo_sex(String periodo_sex) {
		this.periodo_sex = periodo_sex;
	}

	public Boolean getFlag_periodo_alterado() {
		return flag_periodo_alterado;
	}

	public void setFlag_periodo_alterado(Boolean flag_periodo_alterado) {
		this.flag_periodo_alterado = flag_periodo_alterado;
	}

	public String getStatus_contrato() {
		return status_contrato;
	}

	public void setStatus_contrato(String status_contrato) {
		this.status_contrato = status_contrato;
	}

	public String getTexto_periodo_alterado() {
		return texto_periodo_alterado;
	}

	public void setTexto_periodo_alterado(String texto_periodo_alterado) {
		this.texto_periodo_alterado = texto_periodo_alterado;
	}

	public String getUnidade() {
		return unidade;
	}

	public void setUnidade(String unidade) {
		this.unidade = unidade;
	}

	public Boolean getFlag_boleto_emitido() {
		return flag_boleto_emitido;
	}

	public void setFlag_boleto_emitido(Boolean flag_boleto_emitido) {
		this.flag_boleto_emitido = flag_boleto_emitido;
	}

	public Boolean getFlag_primeiro_acesso() {
		return flag_primeiro_acesso;
	}

	public void setFlag_primeiro_acesso(Boolean flag_primeiro_acesso) {
		this.flag_primeiro_acesso = flag_primeiro_acesso;
	}

	public Boolean getFlag_inadimplente() {
		return flag_inadimplente;
	}

	public void setFlag_inadimplente(Boolean flag_inadimplente) {
		this.flag_inadimplente = flag_inadimplente;
	}

	public String getEndereco_ip() {
		return endereco_ip;
	}

	public void setEndereco_ip(String endereco_ip) {
		this.endereco_ip = endereco_ip;
	}

	public String getMes_inicio() {
		return mes_inicio;
	}

	public void setMes_inicio(String mes_inicio) {
		this.mes_inicio = mes_inicio;
	}

	public Double getValor_base() {
		return valor_base;
	}

	public void setValor_base(Double valor_base) {
		this.valor_base = valor_base;
	}

	public String getDesconto() {
		return desconto;
	}

	public void setDesconto(String desconto) {
		this.desconto = desconto;
	}

	public String getAno_entrada() {
		return ano_entrada;
	}

	public void setAno_entrada(String ano_entrada) {
		this.ano_entrada = ano_entrada;
	}

	public String getHistorico_texto_periodo_alterado() {
		return historico_texto_periodo_alterado;
	}

	public void setHistorico_texto_periodo_alterado(String historico_texto_periodo_alterado) {
		this.historico_texto_periodo_alterado = historico_texto_periodo_alterado;
	}
	

	
		
	
	
		
	
}
