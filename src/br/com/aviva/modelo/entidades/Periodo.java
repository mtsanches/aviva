package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="periodo")
public class Periodo implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	private String nome_periodo;
	private int total_horas;
	
	@ManyToOne @JoinColumn(name = "escola_id") 
	private Escola escola;
	
	@ManyToOne @JoinColumn(name = "unidade_id") 
	private Unidade unidade;
	
	private String observacao;
	
	private Boolean flag_ativo;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	// GETTERS E SETTERS
	
	public Periodo(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome_periodo() {
		return nome_periodo;
	}

	public void setNome_periodo(String nome_periodo) {
		this.nome_periodo = nome_periodo;
	}

	public int getTotal_horas() {
		return total_horas;
	}

	public void setTotal_horas(int total_horas) {
		this.total_horas = total_horas;
	}

	public Escola getEscola() {
		return escola;
	}

	public void setEscola(Escola escola) {
		this.escola = escola;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public String getObservacao() {
		return observacao;
	}

	public void setObservacao(String observacao) {
		this.observacao = observacao;
	}

	public Boolean getFlag_ativo() {
		return flag_ativo;
	}

	public void setFlag_ativo(Boolean flag_ativo) {
		this.flag_ativo = flag_ativo;
	}

	public Date getData_criacao() {
		return data_criacao;
	}

	public void setData_criacao(Date data_criacao) {
		this.data_criacao = data_criacao;
	}

	
	

		
	
}
