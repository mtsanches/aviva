package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="bemestar")
public class BemEstar implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_ocorrencia;
	
	private String tipo_bemestar;
	private String descricao;	
	private String status_bemestar;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	// GETTERS E SETTERS
	
	public BemEstar(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getData_ocorrencia() {
		return data_ocorrencia;
	}

	public void setData_ocorrencia(Date data_ocorrencia) {
		this.data_ocorrencia = data_ocorrencia;
	}

	public String getTipo_bemestar() {
		return tipo_bemestar;
	}

	public void setTipo_bemestar(String tipo_bemestar) {
		this.tipo_bemestar = tipo_bemestar;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getStatus_bemestar() {
		return status_bemestar;
	}

	public void setStatus_bemestar(String status_bemestar) {
		this.status_bemestar = status_bemestar;
	}

	

	
		
	
}
