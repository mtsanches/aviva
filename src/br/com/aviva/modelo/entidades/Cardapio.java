package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="cardapio")
public class Cardapio implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	private String tipo_cardapio;
	private String tipo_alimento;
	private String nome_alimento;
	
	@ManyToOne @JoinColumn(name = "turma_id") 
	private Turma turma;
	
	@Basic
	@Temporal(TemporalType.DATE)
	private Date data;
	
	private String dia_semana;
	
	
	// GETTERS E SETTERS
	
	public Cardapio(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTipo_cardapio() {
		return tipo_cardapio;
	}

	public void setTipo_cardapio(String tipo_cardapio) {
		this.tipo_cardapio = tipo_cardapio;
	}

	public String getTipo_alimento() {
		return tipo_alimento;
	}

	public void setTipo_alimento(String tipo_alimento) {
		this.tipo_alimento = tipo_alimento;
	}

	public String getNome_alimento() {
		return nome_alimento;
	}

	public void setNome_alimento(String nome_alimento) {
		this.nome_alimento = nome_alimento;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public Date getData() {
		return data;
	}

	public void setData(Date data) {
		this.data = data;
	}

	public String getDia_semana() {
		return dia_semana;
	}

	public void setDia_semana(String dia_semana) {
		this.dia_semana = dia_semana;
	}

	
	
}
