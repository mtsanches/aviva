package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.List;

import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;
import org.hibernate.mapping.Set;

@Entity
@Table(name="usuario")
public class Usuario implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@Index(name = "nome_INDEX")
	private String nome ;
	
	@Index(name = "login_INDEX")
	@Column(name = "login",unique=true)
	private String login;
	
	private String senha;
	
	@ManyToOne @JoinColumn(name = "perfil_id") 
	private Perfil perfil;
	
	@ManyToOne @JoinColumn(name = "funcao_id") 
	private Funcao funcao;
	
	@ManyToOne @JoinColumn(name = "escola_id") 
	private Escola escola;
	
	@ManyToOne @JoinColumn(name = "unidade_id") 
	private Unidade unidade;
	
	@ManyToOne @JoinColumn(name = "turma_id") 
	private Turma turma;
	
	@ManyToMany(cascade=CascadeType.ALL)
	@JoinTable(
	        name = "usuario_aluno", 
	        joinColumns = { @JoinColumn(name = "usuario_id") }, 
	        inverseJoinColumns = { @JoinColumn(name = "aluno_id") }
	    )
	private List<Aluno> alunos;
	
	private String email;
	
	private Boolean flag_usuario_bloqueado;
	
	//endere�o
	private String logradouro;
	private String numero;
	private String complemento;
	private String bairro;
	private String cidade;
	private String estado;
	private String cep;
	
	private String telefone_residencial;
	private String telefone_celular;
	private String telefone_comercial;
	
	private String cpf;
	private String rg;
	
	private String foto;
	private Date data_nascimento;
	
	private Boolean flag_usuario_master;
	
	private Boolean flag_ativo;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	
	// GETTERS E SETTERS
	
	public Usuario(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getLogin() {
		return login;
	}

	public void setLogin(String login) {
		this.login = login;
	}

	public String getSenha() {
		return senha;
	}

	public void setSenha(String senha) {
		this.senha = senha;
	}

	public Perfil getPerfil() {
		return perfil;
	}

	public void setPerfil(Perfil perfil) {
		this.perfil = perfil;
	}

	public Funcao getFuncao() {
		return funcao;
	}

	public void setFuncao(Funcao funcao) {
		this.funcao = funcao;
	}

	public Escola getEscola() {
		return escola;
	}

	public void setEscola(Escola escola) {
		this.escola = escola;
	}

	public Unidade getUnidade() {
		return unidade;
	}

	public void setUnidade(Unidade unidade) {
		this.unidade = unidade;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public Boolean getFlag_usuario_bloqueado() {
		return flag_usuario_bloqueado;
	}

	public void setFlag_usuario_bloqueado(Boolean flag_usuario_bloqueado) {
		this.flag_usuario_bloqueado = flag_usuario_bloqueado;
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	

	public String getTelefone_residencial() {
		return telefone_residencial;
	}

	public void setTelefone_residencial(String telefone_residencial) {
		this.telefone_residencial = telefone_residencial;
	}

	public String getTelefone_celular() {
		return telefone_celular;
	}

	public void setTelefone_celular(String telefone_celular) {
		this.telefone_celular = telefone_celular;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getRg() {
		return rg;
	}

	public void setRg(String rg) {
		this.rg = rg;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}

	public Date getData_nascimento() {
		return data_nascimento;
	}

	public void setData_nascimento(Date data_nascimento) {
		this.data_nascimento = data_nascimento;
	}

	public Boolean getFlag_usuario_master() {
		return flag_usuario_master;
	}

	public void setFlag_usuario_master(Boolean flag_usuario_master) {
		this.flag_usuario_master = flag_usuario_master;
	}

	public Boolean getFlag_ativo() {
		return flag_ativo;
	}

	public void setFlag_ativo(Boolean flag_ativo) {
		this.flag_ativo = flag_ativo;
	}

	public Date getData_criacao() {
		return data_criacao;
	}

	public void setData_criacao(Date data_criacao) {
		this.data_criacao = data_criacao;
	}

	public List<Aluno> getAlunos() {
		return alunos;
	}

	public void setAlunos(List<Aluno> alunos) {
		this.alunos = alunos;
	}

	public String getTelefone_comercial() {
		return telefone_comercial;
	}

	public void setTelefone_comercial(String telefone_comercial) {
		this.telefone_comercial = telefone_comercial;
	}

	
	

	
	
	

		
	
}
