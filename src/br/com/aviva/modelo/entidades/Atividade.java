package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="atividade")
public class Atividade implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	private String descricao;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_inicio;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_fim;
	
	@ManyToOne @JoinColumn(name = "turma_id") 
	private Turma turma;
	
	
	// GETTERS E SETTERS
	
	public Atividade(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public Date getData_inicio() {
		return data_inicio;
	}

	public void setData_inicio(Date data_inicio) {
		this.data_inicio = data_inicio;
	}

	public Date getData_fim() {
		return data_fim;
	}

	public void setData_fim(Date data_fim) {
		this.data_fim = data_fim;
	}

	public Turma getTurma() {
		return turma;
	}

	public void setTurma(Turma turma) {
		this.turma = turma;
	}

	
	
	
	

		
	
}
