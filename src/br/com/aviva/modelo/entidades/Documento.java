package br.com.aviva.modelo.entidades;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.Index;

@Entity
@Table(name="documento")
public class Documento implements Serializable {

	@Id @GeneratedValue
	private Long id;
	
	@ManyToOne @JoinColumn(name = "aluno_id") 
	private Aluno aluno;
	
	private String tipo_documento;
	private String path_arquivo;
	
	@Basic
	@Temporal(TemporalType.TIMESTAMP)
	private Date data_criacao;
	
	
	// GETTERS E SETTERS
	
	public Documento(){}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Aluno getAluno() {
		return aluno;
	}

	public void setAluno(Aluno aluno) {
		this.aluno = aluno;
	}

	public String getTipo_documento() {
		return tipo_documento;
	}

	public void setTipo_documento(String tipo_documento) {
		this.tipo_documento = tipo_documento;
	}

	public String getPath_arquivo() {
		return path_arquivo;
	}

	public void setPath_arquivo(String path_arquivo) {
		this.path_arquivo = path_arquivo;
	}

	public Date getData_criacao() {
		return data_criacao;
	}

	public void setData_criacao(Date data_criacao) {
		this.data_criacao = data_criacao;
	}

	
	

	
	

		
	
}
