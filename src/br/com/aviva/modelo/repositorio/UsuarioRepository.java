package br.com.aviva.modelo.repositorio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.NoResultException;
import javax.persistence.Query;

import br.com.aviva.modelo.entidades.Contrato;
import br.com.aviva.modelo.entidades.Usuario;

public class UsuarioRepository {

private EntityManager manager;
	
public UsuarioRepository ( EntityManager manager ) {

	this.manager = manager ;
}
		
	public void adiciona (Usuario usuario) {	    	
		this.manager.persist(usuario);
		
	}

	public void remove (Long id) {
		Usuario usuario = this.procura(id);
		this.manager.remove(usuario);
	}
		
	public Usuario atualiza (Usuario usuario) {
		return this.manager.merge(usuario);
	}
		
	public Usuario procura (Long id) {
		return this.manager.find (Usuario.class,id);
	}
	
	public Usuario procuraPorLogin(String login) {
		try{
			Query query = this.manager.createQuery("select x from Usuario x where x.login = :cod_login");
			query.setParameter("cod_login", login);
			return (Usuario) query.getSingleResult();
		} catch (NoResultException nre){
			//Ignore this because as per your logic this is ok!
		}
		return null;				
	}
	
	public Contrato procuraPorContrato(String login) {
		try{
			Query query = this.manager.createQuery("select x from Contrato x where x.login = :cod_login");
			query.setParameter("cod_login", login);
			return (Contrato) query.getSingleResult();
		} catch (NoResultException nre){
			//Ignore this because as per your logic this is ok!
		}
		return null;				
	}
		
	public List <Usuario> getLista() {
		Query query = this.manager.createQuery("select x from Usuario x order by x.id");
		return query.getResultList();
	}
	
	public Boolean validaLoginSenha(String login, String senha) {
		Query query = this.manager.createQuery("select x from Usuario x where (x.login = :login and x.senha = :senha)");
		query.setParameter("login", login);
		query.setParameter("senha", senha);
		Boolean achou = false;
		if (query.getResultList().size() > 0){
			achou = true;
		}
		return achou;
	}
	
		
		
}
