package br.com.aviva.modelo.repositorio;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;

import br.com.aviva.modelo.entidades.Contrato;

public class ContratoRepository {

private EntityManager manager;
	
public ContratoRepository ( EntityManager manager ) {

	this.manager = manager ;
}
		
	public void adiciona (Contrato cliente) {	    	
		this.manager.persist(cliente);
		
	}

	public void remove (Long id) {
		Contrato cliente = this.procura(id);
		this.manager.remove(cliente);
	}
		
	public Contrato atualiza (Contrato cliente) {
		return this.manager.merge(cliente);
	}
		
	public Contrato procura (Long id) {
		return this.manager.find (Contrato.class,id);
	}
	
	public List <Contrato> getLista() {
		Query query = this.manager.createQuery("select x from Contrato x order by x.id");
		return query.getResultList();
	}
	
	public List <Contrato> getListaUnidade(String nome_unidade) {
		Query query = this.manager.createQuery("select x from Contrato x where x.unidade = :nome_unidade order by x.id");
		query.setParameter("nome_unidade", nome_unidade);
		return query.getResultList();
	}
	
	public List <Contrato> getListaClienteLogado(long id_cliente) {
		Query query = this.manager.createQuery("select x from Contrato x where x.id = :codigo_cliente order by x.id");
		query.setParameter("codigo_cliente", id_cliente);
		return query.getResultList();
	}
	
	
	public List <Contrato> getListaNome() {
		Query query = this.manager.createQuery("select x from Contrato x order by x.nome");
		return query.getResultList();
	}
	
	public List<Object> getListaContratosVisualizados() {
		Query query = this.manager.createQuery("select count(x),x.unidade from Contrato x where (x.flag_primeiro_acesso = '1') group by x.unidade");
		return query.getResultList();
	}
		
		
}
