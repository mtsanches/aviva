package br.com.aviva.testes;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.MalformedURLException;

import org.apache.coyote.http2.Stream;

import com.itextpdf.text.BaseColor;
import com.itextpdf.text.Chunk;
import com.itextpdf.text.Document;
import com.itextpdf.text.DocumentException;
import com.itextpdf.text.Font;
import com.itextpdf.text.FontFactory;
import com.itextpdf.text.Image;
import com.itextpdf.text.PageSize;
import com.itextpdf.text.Paragraph;
import com.itextpdf.text.Phrase;
import com.itextpdf.text.pdf.PdfPCell;
import com.itextpdf.text.pdf.PdfPTable;
import com.itextpdf.text.pdf.PdfWriter;

public class TestaPDF {

	public static void main(String[] args) throws DocumentException, MalformedURLException, IOException {
		// TODO Auto-generated method stub
		
		Document document = new Document();
		PdfWriter.getInstance(document, new FileOutputStream("C:\\bkp\\iTextHelloWorld.pdf"));
	
		document.open();
		document.setPageSize(PageSize.A4);
		
		PdfPTable table = new PdfPTable(3);
		
		PdfPCell header = new PdfPCell();
        
		header.setBackgroundColor(BaseColor.LIGHT_GRAY);
        header.setBorderWidth(1);
        header.setPhrase(new Phrase(2));
                
        table.addCell(header);
        
        table.addCell("row 1, col 1");
        table.addCell("row 1, col 2");
        table.addCell("row 1, col 3");
        
        document.add(new Paragraph("Primeiro par�grafo na nova p�gina"));
        
        Font font = FontFactory.getFont(FontFactory.COURIER, 16, BaseColor.BLACK);
		Chunk chunk = new Chunk("Hello World", font);
		
		Image figura = Image.getInstance("C:\\bkp\\imagem.png");
		
		//monta o pdf
		document.add(figura);
		document.add(chunk);
		document.add(table);
		
		document.addCreator("AVIVA");
		document.addSubject("Contrato Rematr�cula 2019");
		
		document.newPage();
		document.add(new Paragraph("Segundo par�grafo na nova p�gina"));

		
		document.close();
		
	}
	
	
}
