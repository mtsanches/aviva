package br.com.aviva.utils;

import java.io.Serializable;

public class ConectorDatabase implements Serializable {

	//Dados para conexao Database MYSQL
	private String conexao_url = "nao_aplicavel";
	private String conexao_usuario = "nao_aplicavel";
	private String conexao_senha = "nao_aplicavel";
	
	//CONEX�O LOCAL
	private String nome_factory = "dblocal";
	
	//CONEX�O PRODU��O
	//private String nome_factory = "dbcloud";
	
	
	public String getConexao_url() {
		return conexao_url;
	}
	public void setConexao_url(String conexao_url) {
		this.conexao_url = conexao_url;
	}
	public String getConexao_usuario() {
		return conexao_usuario;
	}
	public void setConexao_usuario(String conexao_usuario) {
		this.conexao_usuario = conexao_usuario;
	}
	public String getConexao_senha() {
		return conexao_senha;
	}
	public void setConexao_senha(String conexao_senha) {
		this.conexao_senha = conexao_senha;
	}
	public String getNome_factory() {
		return nome_factory;
	}
	public void setNome_factory(String nome_factory) {
		this.nome_factory = nome_factory;
	}
	
}
