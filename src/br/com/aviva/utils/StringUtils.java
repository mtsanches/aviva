package br.com.aviva.utils;

import java.math.BigDecimal;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

public class StringUtils {

	public static String replaceStr(String str, String find, String replace){
		int index = str.indexOf(find);
		while (index>-1){
			str = str.substring(0,index)+ replace +  str.substring(index+find.length(),str.length());
			index = str.indexOf(find);
		}
		return str;
	}
	/**
	 * Retorna string com complemento a esquerda.
	 * @param tamanho int tamanho total da String.
	 * @param compl String complemento a ser inserido.
	 * @param value String valor inicial.
	 * @return String string com complemento a esquerda.
	 */
	public static String lpad(int tamanho, String compl, String value){
		String retorno = value;
		
		if (value!=null && value.length()>tamanho)
			return value.substring(0,tamanho);
		else
		if (tamanho>0 && value!=null && compl!=null){
			for (int i=value.length();i<tamanho;i++){
				retorno = compl + retorno;
			}
		}
		return retorno;
	}
	
	
	/**
	 * Retorna string filtrada.
	 * @param String string.	
	 * @return String string com complemento a esquerda.
	 */
	public static String filter(String string){
		String retorno = "";
		
		if (string!=null && string.trim().length()>0){
			char ascii;
			char space = 32;
			if(string!=null){
				ascii = 40;
				string = string.replace(ascii,space);// Retirando o caractere "("
				ascii = 92;
				string = string.replace(ascii,space);// Retirando o caractere "\"
				//ascii = 47;
				//string = string.replace(ascii,space);// Retirando o caractere "/"
				ascii = 41;
				string = string.replace(ascii,space);// Retirando o caractere ")"
				ascii = 39;
				string = string.replace(ascii,space);// Retirando o caractere "'"
				ascii = 60;
				string = string.replace(ascii,space);// Retirando o caractere "<"
				ascii = 62;
				string = string.replace(ascii,space);// Retirando o caractere ">"
				ascii = 61;
				string = string.replace(ascii,space);// Retirando o caractere "="
				ascii = 42;
				string = string.replace(ascii,space);// Retirando o caractere "*"
				ascii = 34;
				string = string.replace(ascii,space);// Retirando o caractere """
				retorno=string;			
			}
		}
		return retorno.trim();
	}
	/**
	 * Retorna string fomatada xxxx.xxxx.xxxx.xxxx
	 * @param conta string conta.
	 * @return String conta formatadado.
	 */
	public static String formataConta(String conta){
		String formatConta="";
		if (conta.startsWith("000")){
			conta = conta.substring(3,conta.length());
		}
		if(conta.trim().length()>0){
			formatConta = conta.substring(0,4)+".";
			formatConta+= conta.substring(4,8)+".";
			formatConta+=conta.substring(8,12)+".";
			formatConta+=conta.substring(12,conta.length());
		}			
		return formatConta;
	}	
	
	/**
	 * @param cpf string cpf.
	 * @return cpf formatado xxx.xxx.xxx-xx	 
	*/
	public static  String formataCPF(String cpf){
		String formataCpf = "";
		if(cpf.trim().length()>0){
			formataCpf = cpf.substring(0,3)+".";
			formataCpf+= cpf.subSequence(3,6)+".";
			formataCpf+= cpf.subSequence(6,9)+"-";
			formataCpf+= cpf.subSequence(9,cpf.length());
		}		
		return formataCpf;
		
	}
	public static String convCaracteres(String str){
		String caracteres = "��������������������������������������";
		String conversao  = "AAAAAAAACCEEEEEEIIIIIIOOOOOOOOUUUUUUNN";
		for (int i=0;i<caracteres.length();i++){
			String caracter =  caracteres.substring(i,i+1);
			String letra =  conversao.substring(i,i+1);
			if (str.indexOf(caracter)>-1){ 
			   str = str.replaceAll(caracter,letra);
			}
		}
		return str.toUpperCase();
	}
	public static String formataData(String formato, Date data ){
			
		String retorno = "";
		try {
			SimpleDateFormat fmt = new SimpleDateFormat(formato);
			retorno= fmt.format(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	  return retorno; 	
		
	}
	public static Date desFormataData(String formato, String data ){
		
		Date retorno = null;
		try {
			SimpleDateFormat fmt = new SimpleDateFormat(formato);
			retorno= fmt.parse(data);
		} catch (Exception e) {
			e.printStackTrace();
		}
	  return retorno; 	
		
	}

	public static String formataNumero(int decimais, BigDecimal value){
		NumberFormat format = NumberFormat.getInstance();
		BigDecimal bigDecimal = null;	
		if (decimais>0){
			format.setMinimumFractionDigits(decimais);
			format.setMaximumFractionDigits(decimais);
			bigDecimal = value.setScale(decimais,BigDecimal.ROUND_FLOOR); 
		}else{
			bigDecimal = value.setScale(2,BigDecimal.ROUND_FLOOR); 
		}
		return String.valueOf(bigDecimal);
	}

	public static boolean validaCPF(String cpf){
		boolean retorno = false;
		//se o valor for nulo � inv�lido
	    if (cpf==null){
	    	return retorno;
	    }
  	   //O CPF deve possuir 11 digitos. 
		while (cpf.length()<11){
			cpf = "0" + cpf;
		}
		//tratamento de erro valor n�o num�rico
		try{
			//O CPF n�o poder ser zerado.
			if (Double.valueOf(cpf).intValue()==0){
				return retorno;
			}
		}catch(Exception e){
			e.printStackTrace();
			return retorno;
		}
        //Calcula os dois digitos de verifica��o do CPF. }
		for (int tamanho=9;tamanho<=10;tamanho++){
		    int total = 0;
		    int multiplicador = 2;
		    // Utiliza todos os digitos a esquerda do digito verificador sendo calculado. 
		    for (int i=tamanho;i>0;i--){
		    	String dig =cpf.substring(i-1,i); 
			    total = total + Integer.valueOf(dig).intValue() * multiplicador;
			    multiplicador++;
		    }
		    int digito = 11 - total   %  11;
		    int pos = tamanho+1;
	        if (digito > 9)
	          digito = 0;
		    if (digito != Integer.valueOf(cpf.substring(pos-1,pos)).intValue()){
		    	return retorno;
		    }
		}
		return true;
	}
	public static String mascararCartao(String numeroCartao){
		String retorno = "0";
		try {
			while (numeroCartao.length()<19){
				numeroCartao="0" +numeroCartao;
			} 
			if (numeroCartao!=null){
				retorno = numeroCartao;
				retorno = retorno.substring(3,9) + "******" + 
				retorno.substring(15,19); 
			}
		} catch (Exception e) {
			// TODO: handle exception
		}
		return retorno;

	}
	public static String formataCartao(String numeroCartao){
		String retorno = "0";
		try {
			while (numeroCartao.length()<19){
				numeroCartao="0" +numeroCartao;
			} 
			retorno = numeroCartao;
			retorno = retorno.substring(3,7) + "." + 
			retorno.substring(7,11) + "." + 
			retorno.substring(11,15) + "." + 
			retorno.substring(15,19); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		return retorno;
	}
	
	public static void main(String[] args) {             
		System.out.println(StringUtils.formatNumber("198000,05"));
		System.out.println(StringUtils.formatarRG("00217752263"));
				
	}
	public static String formatarRG(String numeroRG) {
		return formatarProposta(removeZerosInicio(numeroRG));
	}
	public static String formatarProposta(String numeroProposta) {
		String retorno = "0";
		try {
			while (numeroProposta.length()<10){
				numeroProposta="0" +numeroProposta;
			} 
			retorno = numeroProposta;
			retorno = retorno.substring(0,3) + "." + 
					  retorno.substring(3,6) + "." + 
					  retorno.substring(6,9) + "-" + 
			retorno.substring(9,10); 
		} catch (Exception e) {
			// TODO: handle exception
		}
		return retorno;
	}
	public static String converteformatoData(String data, String formato, String novoFormato){
		String retorno = data;
		try {
			SimpleDateFormat fmt = new SimpleDateFormat(formato);
			Date dt = fmt.parse(data);
			fmt = new SimpleDateFormat(novoFormato);
			retorno = fmt.format(dt);
		} catch (Exception e) {
			System.out.println("Erro ao formatar data valor=\""+ data +"\" formato=\"" + formato + 
					"\" formato novo=\"" + novoFormato + "\"");
		}
		
		return retorno;
	}
	public static String formatNumber(String numero, boolean simbolo){
		NumberFormat format = NumberFormat.getCurrencyInstance(Locale.GERMANY);
		numero = StringUtils.replaceStr(numero, ".", "");
		numero = StringUtils.replaceStr(numero, ",", ".");
		BigDecimal bigDecimal = new BigDecimal(numero); 
		bigDecimal = bigDecimal.setScale(2,BigDecimal.ROUND_HALF_UP); 
		String valor = format.format(bigDecimal);
		valor = valor.substring(0,valor.length()-2);
		if (simbolo)
			valor = "R$ " + valor; 
		return valor;
	}		
	public static String formatNumber(String numero){
		return formatNumber(numero,true);
	}		
	
	public static String removeZerosInicio(String valor) {
    	String regex = "^0*";
	    return valor.replaceAll(regex, "");
    }

	
}
