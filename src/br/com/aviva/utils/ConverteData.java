package br.com.aviva.utils;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

public class ConverteData {

	public int converteDataToNumber(Date data1){
		
		Date today = data1;
		//System.out.println("Data Atual: " + today);
		DateFormat df = new SimpleDateFormat("yyyyMMdd");
		String reportDate = df.format(today);
		System.out.println("Date String: " + reportDate);
		int dateNumber = Integer.parseInt(reportDate);
		System.out.println("Date Number: " + dateNumber);
		return dateNumber;
		
	}

}
