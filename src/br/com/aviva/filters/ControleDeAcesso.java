package br.com.aviva.filters;

import java.io.IOException;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import br.com.aviva.utils.ConectorDatabase;

@WebFilter(servletNames={"Faces Servlet"})
public class ControleDeAcesso implements Filter {

	private EntityManagerFactory factory ;
	
	
	@Override
	public void doFilter (ServletRequest request ,ServletResponse response ,
			FilterChain chain ) throws IOException , ServletException {
		
		
		HttpServletRequest req = (HttpServletRequest) request ;		
		HttpServletResponse res = (HttpServletResponse) response;  
		
		// CHEGADA
			EntityManager manager = this.factory.createEntityManager();
			req.setAttribute ("EntityManager", manager);
			manager.getTransaction().begin();
		// CHEGADA
			
	   	chain.doFilter(req, res);
        
     // SAIDA
     		try {
     			manager.getTransaction().commit();
     		} catch (Exception e) {
     			manager.getTransaction().rollback();
     		} finally {
     			manager.close();
     		}
     }
	
	
	 
	private boolean precisaAutenticar(String url) {  
	    return !url.contains("index.xhtml") && !url.contains("index_web.xhtml") && !url.contains("index_aprova.xhtml") && !url.endsWith(".css") && !url.endsWith(".js") && !url.endsWith(".jpg")  
	    		&& !url.endsWith(".gif") && !url.contains("javax.faces.resource");  
	} 
	

	@Override
	public void init ( FilterConfig filterConfig ) throws ServletException {
		ConectorDatabase conexao = new ConectorDatabase();		
		this.factory = Persistence.createEntityManagerFactory(conexao.getNome_factory());		
		System.out.println("Iniciou a factory manager v1.0 !!!!!!!!!!");
				
	}
	
	@Override
	public void destroy () {		
		this.factory.close();
	}


}
