package br.com.aviva.filters;

import javax.faces.application.NavigationHandler;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.PhaseEvent;
import javax.faces.event.PhaseId;
import javax.faces.event.PhaseListener;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;


@SuppressWarnings("serial")
public class AuthorizationListener implements PhaseListener {
	
  public void afterPhase(PhaseEvent event) {

	FacesContext facesContext = event.getFacesContext();
    String currentPage = facesContext.getViewRoot().getViewId();
    System.out.println("Pagina Atual: " + currentPage);
    
    boolean isLoginPage = (currentPage.lastIndexOf("index.xhtml") > -1 || currentPage.lastIndexOf("index_web.xhtml") > -1);
    boolean isRecuperaPage = (currentPage.contains("recuperar_senha"));
    boolean isDadosInvalidos = (currentPage.contains("dados_invalidos"));
    boolean isErrorPage = (currentPage.contains("erro"));
        
    try {
    	System.out.println(":::Verificando autentica��o do usu�rio");
    	
    	if (!isLoginPage && !ehautenticado()) { //Se nao for pagina de login e nao esta autenticado
			if(!isDadosInvalidos && !isRecuperaPage && !isErrorPage){			  
			  NavigationHandler nh = facesContext.getApplication().getNavigationHandler();		  
			  nh.handleNavigation(facesContext, null, "loginPage");			  
			}
		}
		
		//if ((currentPage.lastIndexOf("home.xhtml") > -1) && ehuser()){
		//	NavigationHandler nh = facesContext.getApplication().getNavigationHandler();		  
		//	nh.handleNavigation(facesContext, null, "loginPage");
		//}
		
		if (currentPage.lastIndexOf("index.xhtml") > -1 || currentPage.lastIndexOf("index_web.xhtml") > -1){
			FacesContext fc = FacesContext.getCurrentInstance();
     	    ExternalContext ec = fc.getExternalContext(); 
    		HttpSession session = (HttpSession)ec.getSession(true);
    		session.invalidate();
		}
		
	} catch (Throwable e) {
		// TODO Auto-generated catch block
		System.out.println("Ocorreu um ERRO: " + e.getMessage());
		e.printStackTrace();
	}
    
  }

 
  public void beforePhase(PhaseEvent event) {
	  //System.out.println("Entrou no beforePhase"); 
  }

  public PhaseId getPhaseId() {
    return PhaseId.RESTORE_VIEW;
  }
  
  private boolean ehautenticado() throws Throwable
  {
	    Boolean retorno = false;
        
     	    FacesContext fc = FacesContext.getCurrentInstance();
     	    ExternalContext ec = fc.getExternalContext(); 
    		HttpSession session = (HttpSession)ec.getSession(true);   
    		
            if (session.getAttribute("usuario") == null){
            	System.out.println("Usuario da Sessao esta vazio... n�o est� autenticado.");
            	retorno = false;
            } else {
            	System.out.println("Usuario da Sessao nao esta vazio.");
            	retorno = true;
            }    	    
              
        return retorno;
        
  }
  
  private boolean ehadmin() throws Throwable
  {
	    Boolean retorno = false;
        
     	    FacesContext fc = FacesContext.getCurrentInstance();
     	    ExternalContext ec = fc.getExternalContext(); 
    		HttpSession session = (HttpSession)ec.getSession(true);   
    		
            if (session.getAttribute("perfil").equals("ADMIN")){
            	retorno = true;
            } else {
            	retorno = false;
            }    	    
              
        return retorno;
        
  }
  
  private boolean ehuser() throws Throwable
  {
	    Boolean retorno = false;
        
     	    FacesContext fc = FacesContext.getCurrentInstance();
     	    ExternalContext ec = fc.getExternalContext(); 
    		HttpSession session = (HttpSession)ec.getSession(true);   
    		
            if (session.getAttribute("perfil").equals("USUARIO")){
            	retorno = true;
            } else {
            	retorno = false;
            }    	    
              
        return retorno;
        
  }
  

}