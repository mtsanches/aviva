package br.com.aviva.controle;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.Date;
import java.util.logging.Logger;

import javax.faces.application.FacesMessage;
import javax.faces.application.NavigationHandler;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.sql.DataSource;

import com.sendgrid.Method;
import com.sendgrid.Request;
import com.sendgrid.Response;
import com.sendgrid.SendGrid;

import br.com.aviva.modelo.entidades.Contrato;
import br.com.aviva.modelo.entidades.Usuario;
import br.com.aviva.modelo.repositorio.UsuarioRepository;
import br.com.aviva.utils.ConectorDatabase;

@ManagedBean
@SessionScoped
public class AutenticadorBean {

	private static final Logger log = Logger.getLogger(AutenticadorBean.class.getName());
	
	ConectorDatabase conexao = new ConectorDatabase();
	
	private Usuario logon = new Usuario();
	private Contrato contrato = new Contrato();
	
	private Usuario user_cadastro = new Usuario();
	
	
	private String conexao_url = conexao.getConexao_url();
	private String conexao_usuario = conexao.getConexao_usuario();
	private String conexao_senha = conexao.getConexao_senha();
	
	//Usuario
	private String usuario;
	private String senha;
	private String senha2;
	private String dbusername;
	private String dbpassword;
	private String dbperfil;
	private Long id;
	private String nome;
	private String email;
	
    Connection con;
    Statement ps;
    ResultSet rs;
    String SQL_Str;
			
	public void registraSaida(){		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpSession session = (HttpSession)ec.getSession(false);		
		session.removeAttribute("usuario");
		session.removeAttribute("idusuario");
		session.removeAttribute("nomeusuario");
		session.removeAttribute("perfil");
		session.removeAttribute("cliente");
		session.removeAttribute("idcliente");
		session.removeAttribute("tracking");
		session.removeAttribute("status");
		session.invalidate();
		ec.invalidateSession();
		try {
			ec.redirect(ec.getRequestContextPath() + "/index.xhtml");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	

	public void autentica() throws IOException
    {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		EntityManager manager = this.getManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		
		this.logon = repository.procuraPorLogin(this.usuario);
		
		if (this.logon == null){
			this.usuario = "";
        	this.senha = "";			
        	ec.redirect(ec.getRequestContextPath() + "/dados_invalidos.xhtml");
		} else {
			
			//Efetuar tratamento para remover pontos e espa�os da senha
			String senha_limpa = this.senha;
			senha_limpa = senha_limpa.replace(".", "");
			senha_limpa = senha_limpa.replace("-", "").trim();
			this.senha = senha_limpa;
			
			//Trata a Senha informada para remover os zeros a esquerda - n�o s�o necess�rios
			String x = this.senha;
			String temp = "";
			for (int i = 0; i < x.length(); i++) {
				if (!x.substring(i, i + 1).equals("0")) { //checa se chegou no primeiro caracter q n�o � 0
					temp += x.substring(i, x.length()); // temp fica com os valores correspondentes � substring da posi��o atual ate o fim
					break; // sai do la�o
				}
			}
			this.senha = temp;
			
			
			if (this.usuario.equals(this.logon.getLogin()) && this.senha.equals(this.logon.getSenha())){
				
				//Busca os dados do contrato
				if (this.logon.getPerfil().getNome_perfil().equals("USUARIO")) {
					this.contrato = repository.procuraPorContrato(this.usuario);
				}
				
				if (!this.autenticar())
		        {   
					System.out.println("Usuario autenticado pelo Portal");
				
					if (this.contrato.getFlag_inadimplente() == null) {
						//TODO
					} else {
						if (this.contrato.getFlag_inadimplente() == true) {
							ec.redirect(ec.getRequestContextPath() + "/home_msg.xhtml");
						}
					}
					
					
					if (this.logon.getPerfil().getNome_perfil().equals("USUARIO")){
						ec.redirect(ec.getRequestContextPath() + "/home.xhtml");
					} else {
						
						if (this.usuario.equals("avivaweb")) {
							ec.redirect(ec.getRequestContextPath() + "/web/home.xhtml");
						} else {
							ec.redirect(ec.getRequestContextPath() + "/home_admin.xhtml");
						}
						
					}
					
		        } else {
		        	System.out.println("Usuario nao autenticado pelo Portal 1");
		        	this.usuario = "";
		        	this.senha = "";
		        	ec.redirect(ec.getRequestContextPath() + "/index.xhtml");        			        			
		        }
			} else {
				System.out.println("Usuario nao autenticado pelo Portal 3");
				this.usuario = "";
	        	this.senha = "";
				ec.redirect(ec.getRequestContextPath() + "/dados_invalidos.xhtml");
			}
		}        
    }
	
	public void esquecisenha() throws IOException
    {
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		EntityManager manager = this.getManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		this.logon = repository.procuraPorLogin(this.usuario.trim());
		
		if (this.logon != null){
			log.info("Usuario encontrado no sistema. Enviar o email com a senha.");
			log.info("Login = " + this.logon.getLogin());
			log.info("Email = " + this.logon.getEmail());
			try {
				enviaremail(this.logon.getEmail(),this.logon.getSenha());
				ec.redirect(ec.getRequestContextPath() + "/recuperar_senha_sucesso.xhtml");
			} catch (IOException ex) {
				ec.redirect(ec.getRequestContextPath() + "/recuperar_senha_erro.xhtml");
		    }
		} else {		
			log.info("Usuario nao encontrado no sistema.");
			ec.redirect(ec.getRequestContextPath() + "/recuperar_senha_erro.xhtml");  
		}        
    }
	
	public void enviaremail(String email, String senha) throws IOException {
		
		try {
		      SendGrid sg = new SendGrid("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");
		      Request request = new Request();
		      request.setMethod(Method.POST);
		      request.setEndpoint("mail/send");
		      request.setBody("{\"personalizations\":[{\"to\":[{\"email\":\"" + email + "\"}],\"subject\":\"Aviva - Lembrete de Senha\"}],\"from\":{\"email\":\"suporte@avivadi.com.br\"},\"content\":[{\"type\":\"text/plain\",\"value\": \"Conforme solicitado segue sua senha para acesso ao sistema Agenda Aviva: "+ senha +"\"}]}");
		      Response response = sg.api(request);
		      //System.out.println(response.getStatusCode());
		      //System.out.println(response.getBody());
		      //System.out.println(response.getHeaders());
		    } catch (IOException ex) {
		      log.info("ERRO Envio Email: " + ex.getMessage());
		      throw ex;
		    }		
	}
	
	public void gravaData(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date()); // sets calendar time/date
		//TODO Reduzir uma hora quando hor�rio de ver�o
		//cal.add(Calendar.HOUR_OF_DAY, 3); //adiciona 4 horas 
		Date data_hoje = cal.getTime();
		this.user_cadastro.setData_criacao(data_hoje);		
	}

	
	 private boolean autenticar()
     {
           try{
        	   FacesContext fc = FacesContext.getCurrentInstance();
        	   ExternalContext ec = fc.getExternalContext(); 
       		   HttpSession session = (HttpSession)ec.getSession(true);        	   
               session.setAttribute("usuario", this.logon.getLogin());
               session.setAttribute("idusuario", this.logon.getId()); 
               session.setAttribute("nomeusuario", this.logon.getNome());
               session.setAttribute("perfil", this.logon.getPerfil().getNome_perfil());
               session.setAttribute("cliente", "Aviva");
               session.setAttribute("status", this.contrato.getStatus_contrato());
               log.info("Efetuou a autentica��o com o usuario.");
           }catch(Exception e){
                  System.out.println(e.getMessage());
           }
           
           return (!true);
     }
	
	
	
	private EntityManager getManager () {
		FacesContext fc = FacesContext.getCurrentInstance ();
		ExternalContext ec = fc.getExternalContext ();
		HttpServletRequest request = (HttpServletRequest)ec.getRequest ();
		return ( EntityManager ) request . getAttribute ("EntityManager");
	}




	public ConectorDatabase getConexao() {
		return conexao;
	}




	public void setConexao(ConectorDatabase conexao) {
		this.conexao = conexao;
	}




	public String getConexao_url() {
		return conexao_url;
	}




	public void setConexao_url(String conexao_url) {
		this.conexao_url = conexao_url;
	}




	public String getConexao_usuario() {
		return conexao_usuario;
	}




	public void setConexao_usuario(String conexao_usuario) {
		this.conexao_usuario = conexao_usuario;
	}




	public String getConexao_senha() {
		return conexao_senha;
	}




	public void setConexao_senha(String conexao_senha) {
		this.conexao_senha = conexao_senha;
	}




	public String getUsuario() {
		return usuario;
	}




	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}




	public String getSenha() {
		return senha;
	}




	public void setSenha(String senha) {
		this.senha = senha;
	}




	public String getDbusername() {
		return dbusername;
	}




	public void setDbusername(String dbusername) {
		this.dbusername = dbusername;
	}




	public String getDbpassword() {
		return dbpassword;
	}




	public void setDbpassword(String dbpassword) {
		this.dbpassword = dbpassword;
	}




	public String getDbperfil() {
		return dbperfil;
	}




	public void setDbperfil(String dbperfil) {
		this.dbperfil = dbperfil;
	}




	public Long getId() {
		return id;
	}




	public void setId(Long id) {
		this.id = id;
	}




	public String getNome() {
		return nome;
	}




	public void setNome(String nome) {
		this.nome = nome;
	}




	public String getSenha2() {
		return senha2;
	}




	public void setSenha2(String senha2) {
		this.senha2 = senha2;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Contrato getContrato() {
		return contrato;
	}


	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}


	
}
