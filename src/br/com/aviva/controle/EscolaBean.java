package br.com.aviva.controle;

import java.io.IOException;
import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.aviva.modelo.entidades.Contrato;
import br.com.aviva.modelo.repositorio.ContratoRepository;
import br.com.aviva.modelo.repositorio.UsuarioRepository;
import br.com.aviva.services.CepWebService;


@ManagedBean
public class EscolaBean implements Serializable {

	private Contrato contrato = new Contrato();
	private List<Contrato> contratos ;
		
	private Long convenioID;
	private String pagamentoID;
	private String matriculaID;
	private String materialID;
	private String atividadebaleID;
	private String atividadebaleparcelaID;
	private String atividadejudoID;
	private String atividadejudoparcelaID;
	private Boolean concordoID;
	private Boolean usoimagemID;
	
	private boolean disabled; 
	
	public EscolaBean() {
		this.disabled = true;		
	}
	
	public void adiciona () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		this.contrato.setFlag_primeiro_acesso(true);
		
		if (this.contrato.getId() == null) {			
			contratoRepository.adiciona(this.contrato);			
		} else {			
			contratoRepository.atualiza (this.contrato);			
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		this.disabled = true;
		ec.redirect(ec.getRequestContextPath() + "/contrato2.xhtml");
		
	}
	
	public void adiciona_passo2 () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		//boolean retorno = validarPeriodo();
		boolean retorno = false;
		this.contrato.setFlag_primeiro_acesso(true);
		
		if (retorno) {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor preencher o campo com informa��es do novo hor�rio desejado.",null));
		} else {
			
			//Grava log data criacao
			gravaData();
			
			this.pagamentoID = null;
						
			if (this.contrato.getId() == null) {			
				contratoRepository.adiciona(this.contrato);			
			} else {			
				contratoRepository.atualiza (this.contrato);			
			}
			
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			
			if (this.contrato.getStatus_contrato().equals("L") || this.contrato.getStatus_contrato().equals("P") || this.contrato.getStatus_contrato().equals("A")) {
				if (this.contrato.getTexto_periodo_alterado().isEmpty()) {
					ec.redirect(ec.getRequestContextPath() + "/contrato3.xhtml");
				} 
			} 
			
			if (this.contrato.getStatus_contrato().equals("P")) {
				
				if (!this.contrato.getTexto_periodo_alterado().isEmpty()) {
					this.contrato.setStatus_contrato("F");
					this.contrato.setFlag_periodo_alterado(true);
					
					HttpSession session = (HttpSession)ec.getSession(true);        	   
		            session.setAttribute("status", this.contrato.getStatus_contrato());
		            			
					if (this.contrato.getId() == null) {			
						contratoRepository.adiciona(this.contrato);			
					} else {			
						contratoRepository.atualiza (this.contrato);			
					}
					ec.redirect(ec.getRequestContextPath() + "/contrato_emaprovacao.xhtml");
				}
			}
			
		}
		
	}
	
	public void remove () {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long . parseLong ( params.get("id"));
		EntityManager manager = this.getManager();
		System.out.println("Solicitado Remo��o do ID: " + id);
		//UsuarioRepository repository = new UsuarioRepository (manager);
		//repository.remove(id);
		//this.usuarios = null ;
	}
	
	public void adiciona_passo2_comhorario () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		//boolean retorno = validarPeriodo();
		boolean retorno = false;
		this.contrato.setFlag_primeiro_acesso(true);
		
		if (this.contrato.getTexto_periodo_alterado() == "") {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor preencher o campo com informa��es do novo hor�rio desejado.",null));
		} else {
			
			//Grava log data criacao
			gravaData();
			
			this.pagamentoID = null;
						
			if (this.contrato.getId() == null) {			
				contratoRepository.adiciona(this.contrato);			
			} else {			
				contratoRepository.atualiza (this.contrato);			
			}
			
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			
			if (this.contrato.getStatus_contrato().equals("P")) {
				
				if (!this.contrato.getTexto_periodo_alterado().isEmpty()) {
					this.contrato.setStatus_contrato("F");
					this.contrato.setFlag_periodo_alterado(true);
					
					HttpSession session = (HttpSession)ec.getSession(true);        	   
		            session.setAttribute("status", this.contrato.getStatus_contrato());
		            			
					if (this.contrato.getId() == null) {			
						contratoRepository.adiciona(this.contrato);			
					} else {			
						contratoRepository.atualiza (this.contrato);			
					}
					ec.redirect(ec.getRequestContextPath() + "/contrato_emaprovacao.xhtml");
				}
			}
			
		}
		
	}
	
	public void adiciona_passo2_admin () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
						
		if (this.contrato.getId() == null) {			
			contratoRepository.adiciona(this.contrato);			
		} else {			
			contratoRepository.atualiza (this.contrato);			
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		if (this.contrato.getUnidade().equals("Boninas")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_boninas.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Moema")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_moema.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Brooklin")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_brooklin.xhtml");
		}
		
		
		
		
		
	}
	
	public void adiciona_passo3 () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		this.matriculaID = null;
		
		System.out.println("Pagamento ID        : " + this.pagamentoID);
		
		if (this.pagamentoID == null) {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor informar uma op��o de pagamento!", null));
		} else {
			
			if (this.pagamentoID.equals("M")) {
				this.contrato.setMensalidade(true);
				this.contrato.setSemestralidade(false);
				this.contrato.setAnuidade8(false);
				this.contrato.setAnuidade10(false);
			}
			
			if (this.pagamentoID.equals("S")) {
				this.contrato.setSemestralidade(true);
				this.contrato.setMensalidade(false);
				this.contrato.setAnuidade8(false);
				this.contrato.setAnuidade10(false);
			}
			
			if (this.pagamentoID.equals("A8")) {
				this.contrato.setAnuidade8(true);
				this.contrato.setSemestralidade(false);
				this.contrato.setMensalidade(false);
				this.contrato.setAnuidade10(false);
			}
			
			if (this.pagamentoID.equals("A10")) {
				this.contrato.setAnuidade10(true);
				this.contrato.setSemestralidade(false);
				this.contrato.setMensalidade(false);
				this.contrato.setAnuidade8(false);
			}
			
			if (this.contrato.getId() == null) {			
				contratoRepository.adiciona(this.contrato);			
			} else {			
				contratoRepository.atualiza (this.contrato);			
			}

			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato4.xhtml");
		}
				
		
		
	}
	
	public void adiciona_passo4 () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		this.materialID = null;
		
		System.out.println("Matricula ID        : " + this.matriculaID);
		
		if (this.matriculaID == null) {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor informar uma op��o de matricula!", null));
		} else {
			if (this.matriculaID.equals("AV10")) {
				this.contrato.setMatricula_avista6(false);
				this.contrato.setMatricula_avista10(true);
				this.contrato.setMatricula_3parcelas(false);
			}
			if (this.matriculaID.equals("AV6")) {
				this.contrato.setMatricula_avista6(true);
				this.contrato.setMatricula_avista10(false);
				this.contrato.setMatricula_3parcelas(false);
			}
			if (this.matriculaID.equals("3P")) {
				this.contrato.setMatricula_avista6(false);
				this.contrato.setMatricula_avista10(false);
				this.contrato.setMatricula_3parcelas(true);
			}
			
			if (this.contrato.getId() == null) {			
				contratoRepository.adiciona(this.contrato);			
			} else {			
				contratoRepository.atualiza (this.contrato);			
			}
			
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato5.xhtml");
		}
		
		
		
	}
	
	public void adiciona_passo5 () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		this.atividadebaleID = null;
		this.atividadebaleparcelaID = null;
		this.atividadejudoID = null;
		this.atividadejudoparcelaID = null;
		
		if (this.materialID == null) {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor informar uma op��o de pagamento de material!", null));
		} else {
			if (this.materialID.equals("1P")) {
				this.contrato.setMaterial_1parcela(true);
				this.contrato.setMaterial_3parcelas(false);
			}
			
			if (this.materialID.equals("3P")) {
				this.contrato.setMaterial_1parcela(false);
				this.contrato.setMaterial_3parcelas(true);
			}
			
			if (this.contrato.getId() == null) {			
				contratoRepository.adiciona(this.contrato);			
			} else {			
				contratoRepository.atualiza (this.contrato);			
			}
			
			//TODO Se for Grupo G1 pular para contrato7.xhtml
			if (this.contrato.getGrupo().contains("1")) {
				FacesContext fc = FacesContext.getCurrentInstance();
				ExternalContext ec = fc.getExternalContext();
				ec.redirect(ec.getRequestContextPath() + "/contrato7.xhtml");
			}
			
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato6.xhtml");
			
			
		}
		
		
		
	}
	
	public void adiciona_passo6 () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		//Avalia Bale e Judo
		if (this.contrato.getGrupo().equals("3") || this.contrato.getGrupo().equals("4") || this.contrato.getGrupo().equals("5")) {
			if (this.atividadebaleID == null || this.atividadejudoID == null) {
				FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor informar SIM ou N�O para as op��es de atividade extra!", null));				
			} else {
				if (this.atividadebaleID.equals("S1")) {
					this.contrato.setAtividade_bale(true);
					this.contrato.setAtividade_bale_1parcela(true);
					this.contrato.setAtividade_bale_10parcelas(false);
				}
				
				if (this.atividadebaleID.equals("S10")) {
					this.contrato.setAtividade_bale(true);
					this.contrato.setAtividade_bale_1parcela(false);
					this.contrato.setAtividade_bale_10parcelas(true);
				}
				
				if (this.atividadebaleID.equals("N")) {
					this.contrato.setAtividade_bale(false);
					this.contrato.setAtividade_bale_1parcela(false);
					this.contrato.setAtividade_bale_10parcelas(false);
				}
				
				if (this.atividadejudoID.equals("S1")) {
					this.contrato.setAtividade_judo(true);
					this.contrato.setAtividade_judo_1parcela(true);
					this.contrato.setAtividade_judo_10parcelas(false);
				}
				
				if (this.atividadejudoID.equals("S10")) {
					this.contrato.setAtividade_judo(true);
					this.contrato.setAtividade_judo_1parcela(false);
					this.contrato.setAtividade_judo_10parcelas(true);
				}
				
				if (this.atividadejudoID.equals("N")) {
					this.contrato.setAtividade_judo(false);
					this.contrato.setAtividade_judo_1parcela(false);
					this.contrato.setAtividade_judo_10parcelas(false);
				}
				
				if (this.contrato.getId() == null) {			
					contratoRepository.adiciona(this.contrato);			
				} else {			
					contratoRepository.atualiza (this.contrato);			
				}
					
				FacesContext fc = FacesContext.getCurrentInstance();
				ExternalContext ec = fc.getExternalContext();
				ec.redirect(ec.getRequestContextPath() + "/contrato7.xhtml");
			}
		} 
			
		
		//Avalia apenas Balet
		if (this.contrato.getGrupo().equals("2")) {
			if (this.atividadebaleID == null ) {
				FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Favor informar SIM ou N�O para as op��es de atividade extra!", null));				
			} else {
				
				if (this.atividadebaleID.equals("S1")) {
					this.contrato.setAtividade_bale(true);
					this.contrato.setAtividade_bale_1parcela(true);
					this.contrato.setAtividade_bale_10parcelas(false);
				}
				
				if (this.atividadebaleID.equals("S10")) {
					this.contrato.setAtividade_bale(true);
					this.contrato.setAtividade_bale_1parcela(false);
					this.contrato.setAtividade_bale_10parcelas(true);
				}
				
				if (this.atividadebaleID.equals("N")) {
					this.contrato.setAtividade_bale(false);
					this.contrato.setAtividade_bale_1parcela(false);
					this.contrato.setAtividade_bale_10parcelas(false);
				}
				
				if (this.contrato.getId() == null) {			
					contratoRepository.adiciona(this.contrato);			
				} else {			
					contratoRepository.atualiza (this.contrato);			
				}
					
				FacesContext fc = FacesContext.getCurrentInstance();
				ExternalContext ec = fc.getExternalContext();
				ec.redirect(ec.getRequestContextPath() + "/contrato7.xhtml");
				
			}
		} 	
			
			
	}
	
	public void adiciona_passo7 () throws IOException {
		
		EntityManager manager = this.getManager();
		ContratoRepository contratoRepository = new ContratoRepository(manager);
		
		if (!this.concordoID) {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Para prosseguir, favor concordar com o contrato.", null));
		
		} else {
			
			if (this.usoimagemID == null) {
				FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_ERROR,"Para prosseguir favor selecionar uma op��o se autoriza ou n�o o uso de imagem.", null));
			} else {
				
				if (this.concordoID) {
					this.contrato.setFlag_concordo_contrato(true);
				} else {
					this.contrato.setFlag_concordo_contrato(false);
				}
				
				if (this.usoimagemID) {
					this.contrato.setUso_imagem(true);
				} else {
					this.contrato.setUso_imagem(false);
				}
				
				//STATUS DOS CONTRATOS
				//PENDENTE, FINALIZADO, APROVADO
				this.contrato.setStatus_contrato("F");
				
				//Grava log data da contrata��o no contrato
				gravaData();
				
				//Grava o endere�o IP
				HttpServletRequest request = (HttpServletRequest) FacesContext.getCurrentInstance().getExternalContext().getRequest();
				String ipAddress = request.getHeader("X-FORWARDED-FOR");
				if (ipAddress == null) {
				    ipAddress = request.getRemoteAddr();
				}
				
				this.contrato.setEndereco_ip(ipAddress);
				
				if (this.contrato.getId() == null) {			
					contratoRepository.adiciona(this.contrato);			
				} else {			
					contratoRepository.atualiza (this.contrato);			
				}
				
				FacesContext fc = FacesContext.getCurrentInstance();
				ExternalContext ec = fc.getExternalContext();
				HttpSession session = (HttpSession)ec.getSession(true);        	   
	            session.setAttribute("status", this.contrato.getStatus_contrato());
				
				ec.redirect(ec.getRequestContextPath() + "/contrato_finalizado.xhtml");
				
			}
			
			
		}
				
		
		
	}
	
	public void adiciona_passo8 () throws IOException {
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		ec.redirect(ec.getRequestContextPath() + "/home.xhtml");
		
	}
	
	public void preparaAlteracao () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		ec.redirect(ec.getRequestContextPath() + "/contrato.xhtml");
	}
	
	public void preparaAlteracao_admin () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		ec.redirect(ec.getRequestContextPath() + "/contrato2_admin.xhtml");
	}
	
	public void preparaAlteracao_resumo () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		ec.redirect(ec.getRequestContextPath() + "/contrato2_telaresumo.xhtml");
	}
	
	
	
	public void preparaImpressao () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		if (this.contrato.getFlag_concordo_contrato()) {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato_impressao.xhtml");
		} else {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato_nao_impressao.xhtml");
		}
		
	}
	
	public void preparaImpressaoQuadroResumo () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		ec.redirect(ec.getRequestContextPath() + "/contrato_quadro_resumo.xhtml");
		
	}
	
	public void gravaData(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date()); // sets calendar time/date
		//TODO Reduzir uma hora quando hor�rio de ver�o
		cal.add(Calendar.HOUR_OF_DAY, -3); //reduz 3 horas 
		Date data_hoje = cal.getTime();
		contrato.setData_contratacao(data_hoje);		
	}
	
	public void verificarCep() {
		System.out.println("Verificando CEP...: " + this.contrato.getCep());
		BuscaLogradouro(this.contrato.getCep());
		System.out.println("CEP encontrado e atualizado!");		
	}
	
	public void validarCpf() {
		System.out.println("Validando o CPF...: " + this.contrato.getCpf());
		boolean validacpf = calculaCPF(this.contrato.getCpf());
		System.out.println("CPF v�lido: " + validacpf);
		
		if (!validacpf) {
			System.out.println("Entrou no IF de Mensagem de CPF inv�lido");
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage("CPF n�o � v�lido !!!"));
		}
	}
	
	public boolean validarPeriodo() {
		
		boolean retorno = false;
		String periodo1 = this.contrato.getHora_entrada();
		String periodo2 = this.contrato.getHora_saida();
		
		periodo1 = periodo1.replace(":", "");
		periodo2 = periodo2.replace(":", "");
		
		int per1 = Integer.parseInt(periodo1);
		int per2 = Integer.parseInt(periodo2);
		
		if (per1 >= per2) {
			System.out.println("Horario de Entrada maior ou igual ao Horario de Saida");
			retorno = true;
		}
		return retorno;
	
	}
	
	public void BuscaLogradouro(String cep) {
        try {
            
            CepWebService cepWebService = new CepWebService(cep);
                
            if (cepWebService.getResultado()==1) {
            	this.contrato.setEndereco(cepWebService.getTipo_logradouro() + " " + cepWebService.getLogradouro());
            	this.contrato.setBairro(cepWebService.getBairro());
            	this.contrato.setCidade(cepWebService.getCidade());
            	this.contrato.setUf(cepWebService.getEstado());
            	
                System.out.println(cepWebService.getResultado());
                System.out.println(cepWebService.getResultado_txt());
            }
            else {
                System.out.println("Servidor nao esta respondendo.");
            }
            
        }
        catch (Exception ex) {
            ex.printStackTrace();
        } 
       
    } 
	
	public boolean calculaCPF(String strCpf) {  
		
		strCpf = strCpf.replace(".", "");
		strCpf = strCpf.replace(" ", "");
		strCpf = strCpf.replace("-", "").trim();
		
        if (strCpf.equals("")) {  
            return false;  
        }  
        int d1, d2;  
        int digito1, digito2, resto;  
        int digitoCPF;  
        String nDigResult;  
        d1 = d2 = 0;  
        digito1 = digito2 = resto = 0;  
        for (int nCount = 1; nCount < strCpf.length() - 1; nCount++) {  
            digitoCPF = Integer.valueOf(strCpf.substring(nCount - 1, nCount)).intValue();  
            //multiplique a ultima casa por 2 a seguinte por 3 a seguinte por 4 e assim por diante.  
            d1 = d1 + (11 - nCount) * digitoCPF;  
            //para o segundo digito repita o procedimento incluindo o primeiro digito calculado no passo anterior.  
            d2 = d2 + (12 - nCount) * digitoCPF;  
        }  
        //Primeiro resto da divis�o por 11.  
        resto = (d1 % 11);  
        //Se o resultado for 0 ou 1 o digito � 0 caso contr�rio o digito � 11 menos o resultado anterior.  
        if (resto < 2) {  
            digito1 = 0;  
        } else {  
            digito1 = 11 - resto;  
        }  
        d2 += 2 * digito1;  
        //Segundo resto da divis�o por 11.  
        resto = (d2 % 11);  
        //Se o resultado for 0 ou 1 o digito � 0 caso contr�rio o digito � 11 menos o resultado anterior.  
        if (resto < 2) {  
            digito2 = 0;  
        } else {  
            digito2 = 11 - resto;  
        }  
        //Digito verificador do CPF que est� sendo validado.  
        String nDigVerific = strCpf.substring(strCpf.length() - 2, strCpf.length());  
        //Concatenando o primeiro resto com o segundo.  
        nDigResult = String.valueOf(digito1) + String.valueOf(digito2);  
        //comparar o digito verificador do cpf com o primeiro resto + o segundo resto.  
        return nDigVerific.equals(nDigResult);  
    }  
	
	public List <Contrato> getContratos() {		
			if (this.contratos == null) {
				EntityManager manager = this.getManager();
				ContratoRepository repository = new ContratoRepository(manager);
				this.contratos = repository.getLista();
			}
			return this.contratos ;		
	}
	
	public List <Contrato> getClienteLogado() {		
		if (this.contratos == null) {
			EntityManager manager = this.getManager();
			ContratoRepository repository = new ContratoRepository(manager);
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			HttpSession session = (HttpSession)ec.getSession(false);
			Long idcliente = (Long) session.getAttribute("idcliente");
			this.contratos = repository.getListaClienteLogado(idcliente);
		}
		return this.contratos ;		
}
	
	
	private EntityManager getManager () {
		FacesContext fc = FacesContext.getCurrentInstance ();
		ExternalContext ec = fc.getExternalContext ();
		HttpServletRequest request = (HttpServletRequest)ec.getRequest ();
		return ( EntityManager ) request . getAttribute ("EntityManager");
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

	public Long getConvenioID() {
		return convenioID;
	}

	public void setConvenioID(Long convenioID) {
		this.convenioID = convenioID;
	}

	public void setContratos(List<Contrato> contratos) {
		this.contratos = contratos;
	}
	
	public void changeButton(ActionEvent event){
         this.disabled = false;
    } 
	
     public boolean isDisabled() {
         return disabled;
     }

     public void setDisabled(boolean disabled) {
          this.disabled = disabled;
     }

	public String getPagamentoID() {
		return pagamentoID;
	}

	public void setPagamentoID(String pagamentoID) {
		this.pagamentoID = pagamentoID;
	}

	public String getMatriculaID() {
		return matriculaID;
	}

	public void setMatriculaID(String matriculaID) {
		this.matriculaID = matriculaID;
	}

	public String getMaterialID() {
		return materialID;
	}

	public void setMaterialID(String materialID) {
		this.materialID = materialID;
	}

	public String getAtividadebaleID() {
		return atividadebaleID;
	}

	public void setAtividadebaleID(String atividadebaleID) {
		this.atividadebaleID = atividadebaleID;
	}

	public String getAtividadejudoID() {
		return atividadejudoID;
	}

	public void setAtividadejudoID(String atividadejudoID) {
		this.atividadejudoID = atividadejudoID;
	}

	public String getAtividadebaleparcelaID() {
		return atividadebaleparcelaID;
	}

	public void setAtividadebaleparcelaID(String atividadebaleparcelaID) {
		this.atividadebaleparcelaID = atividadebaleparcelaID;
	}

	public String getAtividadejudoparcelaID() {
		return atividadejudoparcelaID;
	}

	public void setAtividadejudoparcelaID(String atividadejudoparcelaID) {
		this.atividadejudoparcelaID = atividadejudoparcelaID;
	}

	public Boolean getConcordoID() {
		return concordoID;
	}

	public void setConcordoID(Boolean concordoID) {
		this.concordoID = concordoID;
	}

	public Boolean getUsoimagemID() {
		return usoimagemID;
	}

	public void setUsoimagemID(Boolean usoimagemID) {
		this.usoimagemID = usoimagemID;
	}

	
	

	
	
}
