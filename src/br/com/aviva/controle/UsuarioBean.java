package br.com.aviva.controle;

import java.io.Serializable;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.aviva.modelo.entidades.Usuario;
import br.com.aviva.modelo.repositorio.UsuarioRepository;


@ManagedBean
public class UsuarioBean implements Serializable {

	private Usuario usuario = new Usuario();
	private List<Usuario> usuarios ;
	private Long clienteID;
	private String senhaatualID;
	private String senhanovaID;
	private String senhanova2ID;
	
	public void adiciona () {
		System.out.println("Inicio Adiciona Usuario");
		EntityManager manager = this.getManager();
		UsuarioRepository usuarioRepository = new UsuarioRepository (manager);
		
		//Grava log data criacao
		gravaData();
				
		if (this.usuario.getId() == null) {			
			usuarioRepository.adiciona(this.usuario);			
		} else {			
			usuarioRepository.atualiza (this.usuario);			
		}
		this.usuario = new Usuario();
		this.usuarios = null;
		this.clienteID = null;
		FacesContext.getCurrentInstance().addMessage("form", new FacesMessage("Informa��es gravadas com sucesso!"));
		System.out.println("Fim Adiciona Usuario");
	}
	
	public void trocasenha() {
		EntityManager manager = this.getManager();
		UsuarioRepository usuarioRepository = new UsuarioRepository (manager);
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		HttpSession session = (HttpSession)ec.getSession(false);
		String login = (String) session.getAttribute("usuario");
		System.out.println("Login: " + login);
		Usuario dados_usuario = usuarioRepository.procuraPorLogin(login);
		Boolean valida = usuarioRepository.validaLoginSenha(login, this.senhaatualID);
		if (valida){
			if (this.getSenhanovaID().equals(this.getSenhanova2ID())){
				dados_usuario.setSenha(this.getSenhanovaID());
				usuarioRepository.atualiza(dados_usuario);
				FacesContext.getCurrentInstance().addMessage("form", new FacesMessage("Senha alterada com sucesso!"));
			} else {
				FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_WARN,"Confirma��o da senha incorreta!",null));
			}			
		} else {
			FacesContext.getCurrentInstance().addMessage("form", new FacesMessage(FacesMessage.SEVERITY_WARN,"Sua senha atual informada est� incorreta!",null));
		}
		this.senhaatualID = "";
		this.senhanovaID = "";
		this.senhanova2ID = "";
		
	}
	
	public void preparaAlteracao () {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		UsuarioRepository repository = new UsuarioRepository(manager);
		this.usuario = repository.procura(id);
	}
	
	public void remove () {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long . parseLong ( params.get("id"));
		EntityManager manager = this.getManager();
		UsuarioRepository repository = new UsuarioRepository (manager);
		repository.remove(id);
		this.usuarios = null ;
	}
	
	public void gravaData(){
		Calendar cal = Calendar.getInstance();
		cal.setTime(new Date()); // sets calendar time/date
		//TODO Reduzir uma hora quando hor�rio de ver�o
		//cal.add(Calendar.HOUR_OF_DAY, 3); //adiciona 4 horas 
		Date data_hoje = cal.getTime();
		usuario.setData_criacao(data_hoje);		
	}
	
	public List <Usuario> getUsuarios() {		
			if (this.usuarios == null) {
				EntityManager manager = this.getManager();
				UsuarioRepository repository = new UsuarioRepository(manager);
				this.usuarios = repository.getLista();
			}
			return this.usuarios ;		
	}
	
	private EntityManager getManager () {
		FacesContext fc = FacesContext.getCurrentInstance ();
		ExternalContext ec = fc.getExternalContext ();
		HttpServletRequest request = (HttpServletRequest)ec.getRequest ();
		return ( EntityManager ) request . getAttribute ("EntityManager");
	}

	public Usuario getUsuario() {
		return usuario;
	}

	public void setUsuario(Usuario usuario) {
		this.usuario = usuario;
	}

	public void setUsuarios(List<Usuario> usuarios) {
		this.usuarios = usuarios;
	}

	public Long getClienteID() {
		return clienteID;
	}

	public void setClienteID(Long clienteID) {
		this.clienteID = clienteID;
	}

	public String getSenhaatualID() {
		return senhaatualID;
	}

	public void setSenhaatualID(String senhaatualID) {
		this.senhaatualID = senhaatualID;
	}

	public String getSenhanovaID() {
		return senhanovaID;
	}

	public void setSenhanovaID(String senhanovaID) {
		this.senhanovaID = senhanovaID;
	}

	public String getSenhanova2ID() {
		return senhanova2ID;
	}

	public void setSenhanova2ID(String senhanova2id) {
		senhanova2ID = senhanova2id;
	}

	
	
	
}
