package br.com.aviva.controle;

import java.io.IOException;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.bean.ViewScoped;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.ActionEvent;
import javax.persistence.EntityManager;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import br.com.aviva.modelo.entidades.Contrato;
import br.com.aviva.modelo.repositorio.ContratoRepository;
import br.com.aviva.services.CepWebService;

@ManagedBean
public class ListaContratoBean implements Serializable {

	private Contrato contrato = new Contrato();
	private List<Contrato> contratos ;
		
	public void confirmacaoBoleto () throws IOException {
		
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		
		if (this.contrato.getFlag_boleto_emitido() == true) {
			this.contrato.setFlag_boleto_emitido(false);
		} else {
			this.contrato.setFlag_boleto_emitido(true);
		}
		
		if (this.contrato.getId() == null) {			
			repository.adiciona(this.contrato);			
		} else {			
			repository.atualiza (this.contrato);			
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		if (this.contrato.getUnidade().equals("Boninas")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_boninas.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Moema")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_moema.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Brooklin")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_brooklin.xhtml");
		}
		
		
	}
	
	public void preparaAprovacao () throws IOException {
		
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		
		this.contrato.setStatus_contrato("A");
		this.contrato.setFlag_periodo_alterado(false);
		this.contrato.setTexto_periodo_alterado("");
		
		if (this.contrato.getId() == null) {			
			repository.adiciona(this.contrato);			
		} else {			
			repository.atualiza (this.contrato);			
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		if (this.contrato.getUnidade().equals("Boninas")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_boninas.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Moema")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_moema.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Brooklin")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_brooklin.xhtml");
		}
			
	}
	
	public void preparaAprovacaoHorario () throws IOException {
		
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		
		this.contrato.setStatus_contrato("L");
		this.contrato.setFlag_periodo_alterado(false);
		this.contrato.setTexto_periodo_alterado("");
		
		if (this.contrato.getId() == null) {			
			repository.adiciona(this.contrato);			
		} else {			
			repository.atualiza (this.contrato);			
		}
		
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		
		if (this.contrato.getUnidade().equals("Boninas")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_boninas.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Moema")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_moema.xhtml");
		}
		
		if (this.contrato.getUnidade().equals("Brooklin")) {
			ec.redirect(ec.getRequestContextPath() + "/lista_contratos_brooklin.xhtml");
		}
	}

	public void preparaAlteracao () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		FacesContext fc = FacesContext.getCurrentInstance();
		ExternalContext ec = fc.getExternalContext();
		ec.redirect(ec.getRequestContextPath() + "/contrato.xhtml");
	}
	
	public void preparaImpressao () throws IOException {
		Map <String,String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		Long id = Long.parseLong (params.get("id"));
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		this.contrato = repository.procura(id);
		System.out.println("==========================================================");
		System.out.println("Contrato encontrado: " + this.contrato.getId());
		if (this.contrato.getFlag_concordo_contrato()) {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato_impressao.xhtml");
		} else {
			FacesContext fc = FacesContext.getCurrentInstance();
			ExternalContext ec = fc.getExternalContext();
			ec.redirect(ec.getRequestContextPath() + "/contrato_nao_impressao.xhtml");
		}
		
	}
	
	public List <Contrato> getContratos() {		
			if (this.contratos == null) {
				EntityManager manager = this.getManager();
				ContratoRepository repository = new ContratoRepository(manager);
				this.contratos = repository.getLista();
			}
			return this.contratos ;		
	}
	
	public List <Contrato> getContratosBoninas() {		
		if (this.contratos == null) {
			EntityManager manager = this.getManager();
			ContratoRepository repository = new ContratoRepository(manager);
			this.contratos = repository.getListaUnidade("Boninas");
		}
		return this.contratos ;		
	}
	
	public List <Contrato> getContratosMoema() {		
		if (this.contratos == null) {
			EntityManager manager = this.getManager();
			ContratoRepository repository = new ContratoRepository(manager);
			this.contratos = repository.getListaUnidade("Moema");
		}
		return this.contratos ;		
	}
	
	public List <Contrato> getContratosBrooklin() {		
		if (this.contratos == null) {
			EntityManager manager = this.getManager();
			ContratoRepository repository = new ContratoRepository(manager);
			this.contratos = repository.getListaUnidade("Brooklin");
		}
		return this.contratos ;		
	}
	
	public List<Object> getContratosVisualizados() {		
		
		List<Object> retorno = new ArrayList();
		EntityManager manager = this.getManager();
		ContratoRepository repository = new ContratoRepository(manager);
		retorno = repository.getListaContratosVisualizados();
		
		//Varre o objeto retornado do SQL
		Iterator<Object> ite =retorno.iterator();
		
		System.out.println("Passou aqui...");
		while (ite.hasNext()) {
            Object[] result = (Object[]) ite.next();
            long total = (long) result[0];
            String unidade = (String) result[1];
            System.out.println("Total: " + total);
            System.out.println("Unidade: " + unidade);
            }
		
		return retorno ;		
	}
	
	
	private EntityManager getManager () {
		FacesContext fc = FacesContext.getCurrentInstance ();
		ExternalContext ec = fc.getExternalContext ();
		HttpServletRequest request = (HttpServletRequest)ec.getRequest ();
		return ( EntityManager ) request . getAttribute ("EntityManager");
	}

	public Contrato getContrato() {
		return contrato;
	}

	public void setContrato(Contrato contrato) {
		this.contrato = contrato;
	}

		
	
}
